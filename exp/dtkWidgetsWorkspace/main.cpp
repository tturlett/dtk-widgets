// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtWidgets>

#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>

// ///////////////////////////////////////////////////////////////////
// dummy workspace
// ///////////////////////////////////////////////////////////////////

class dummyWorkspaceA: public dtkWidgetsWorkspace
{
        Q_OBJECT

public:
    dummyWorkspaceA(QWidget *parent = nullptr) {

        QLabel *label =  new QLabel("my dummy workspace with menu", this);
        label->setAlignment(Qt::AlignCenter);

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(30, 30, 30, 30);
        layout->setSpacing(0);
        layout->addWidget(label);

// /////////////////////////////////////////////////////////////////////////////
// As an overlay: has to be declared last
// /////////////////////////////////////////////////////////////////////////////

        dtkWidgetsMenu *menu_1 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 1");
        dtkWidgetsMenu *menu_2 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 2");
        dtkWidgetsMenu *menu_3 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 3");

        this->menubar = new dtkWidgetsMenuBar(this);
        this->menubar->addMenu(menu_1);
        this->menubar->addMenu(menu_2);
        this->menubar->addMenu(menu_3);
        this->menubar->touch();
    };

    ~dummyWorkspaceA(void) {};

public:
    void resizeEvent(QResizeEvent *event) override;

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};

private:
    dtkWidgetsMenuBar *menubar;
};

void dummyWorkspaceA::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    this->menubar->setFixedHeight(event->size().height());
}

static dtkWidgetsWorkspace *dummyWorkspaceCreatorA(void) {
    return new dummyWorkspaceA;
}

class dummyWorkspaceB: public dtkWidgetsWorkspace
{
        Q_OBJECT

public:
    dummyWorkspaceB(QWidget *parent = nullptr) {

        QLabel *label =  new QLabel("dummy workspace B", this);
        label->setAlignment(Qt::AlignCenter);

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(label);

        this->setObjectName("dummy workspace B");
    };

    ~dummyWorkspaceB(void) {};

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};
};

static dtkWidgetsWorkspace *dummyWorkspaceCreatorB(void) {
    return new dummyWorkspaceB;
}

class dummyWorkspaceC: public dtkWidgetsWorkspace
{
        Q_OBJECT

public:
    dummyWorkspaceC(QWidget *parent = nullptr) {

        QLabel *label = new QLabel(this);
        label->setPixmap(QPixmap(":/logo.jpg"));

        QScrollArea *scroll = new QScrollArea(this);
        scroll->setWidget(label);

        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        layout->setSpacing(0);
        layout->addWidget(scroll);

        this->setObjectName("dummy workspace C");
    };

    ~dummyWorkspaceC(void) {};

public:
    void enter(void) override {}
    void leave(void) override {}

public slots:
    void apply(void) override {};
};

static dtkWidgetsWorkspace *dummyWorkspaceCreatorC(void) {
    return new dummyWorkspaceC;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummyWindow : public QFrame
{
    Q_OBJECT

public:
    dummyWindow(QWidget *parent = nullptr);

public:
    QSize sizeHint(void) const { return QSize(640, 480); }

private:
    dtkWidgetsMenuBar *menubar;
    dtkWidgetsMenu *menu_1;
    dtkWidgetsMenu *menu_2;
    dtkWidgetsMenu *menu_3;

public:
    void resizeEvent(QResizeEvent *event);

public:
    QStackedWidget *stack;
};

dummyWindow::dummyWindow(QWidget *parent) : QFrame(parent)
{
    this->stack = new QStackedWidget(this);
    this->stack->addWidget(dtk::widgets::workspace::pluginFactory().create("dummyWorkspaceC"));
    this->stack->addWidget(dtk::widgets::workspace::pluginFactory().create("dummyWorkspaceB"));

    dtkWidgetsWorkspaceStackBar *stack_bar = new dtkWidgetsWorkspaceStackBar(this);
    stack_bar->setStack(this->stack);

    this->menubar = new dtkWidgetsMenuBar(this);
    this->menu_1 = new dtkWidgetsMenu(fa::globe, "Workspaces");
    this->menu_2 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 2");
    this->menu_3 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 3");
    this->menu_1->addItem(new dtkWidgetsMenuItemWorkspace("Workspaces", stack_bar));

    this->menubar->addMenu(this->menu_1);
    this->menubar->addMenu(this->menu_2);
    this->menubar->addMenu(this->menu_3);
    this->menubar->touch();

    QHBoxLayout *l = new QHBoxLayout(this);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->addWidget(this->menubar);
    l->addWidget(this->stack);
}

void dummyWindow::resizeEvent(QResizeEvent *event)
{
    QFrame::resizeEvent(event);

    this->menubar->setFixedHeight(event->size().height());
}

class dummyWindowBar : public QFrame
{
    Q_OBJECT

public:
    dummyWindowBar(QWidget *parent = nullptr);

public:
    QSize sizeHint(void) const { return QSize(640, 480); }

public:
    QStackedWidget *stack;
};

dummyWindowBar::dummyWindowBar(QWidget *parent) : QFrame(parent)
{
    this->stack = new QStackedWidget(this);

    dtkWidgetsWorkspaceBar *bar = new dtkWidgetsWorkspaceBar;
    bar->setStack(this->stack);

    // For a dynamic bar with a botton that pops a menu to create new workspace:
    //
    // bar->addWorkspaceInMenu("dummyWorkspaceA", "My Dummy Workspace A");
    // bar->addWorkspaceInMenu("dummyWorkspaceB", "My Dummy Workspace B", fa::image, "category1");
    // bar->addWorkspaceInMenu("dummyWorkspaceC", "dtk", fa::image, "category1");

    // Another option is to build automatically the bar from the factory keys
    // bar->buildFromFactory();

    // here we use a static workspace bar

    bar->setDynamic(false);
    bar->createWorkspace("My Dummy Workspace A", "dummyWorkspaceA", false );
    bar->createWorkspace("My Dummy Workspace B", "dummyWorkspaceB", false );

    QVBoxLayout *l = new QVBoxLayout(this);
    l->setContentsMargins(0, 0, 0, 0);
    l->setSpacing(0);
    l->addWidget(stack);
    l->addWidget(bar);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dtkFontAwesome::instance()->initFontAwesome();

    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceA", dummyWorkspaceCreatorA);
    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceB", dummyWorkspaceCreatorB);
    dtk::widgets::workspace::pluginFactory().record("dummyWorkspaceC", dummyWorkspaceCreatorC);

    dummyWindow *window = new dummyWindow;
    window->setWindowTitle("One menu, multiple workspaces");
    window->show();
    window->raise();

    dummyWindowBar *windowBar = new dummyWindowBar;
    windowBar->setWindowTitle("Multiple menus, multiple workspaces: one menu per workspace");
    windowBar->show();
    windowBar->raise();
    windowBar->move(windowBar->pos() + QPoint(50, 50));

    dtkThemesEngine::instance()->apply();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
