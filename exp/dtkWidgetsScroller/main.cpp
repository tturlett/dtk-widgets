// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkFonts>
#include <dtkWidgets>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsScrollerHUDItem : public QLabel
{
    Q_OBJECT

public:
    dtkWidgetsScrollerHUDItem(int id, QWidget *parent = nullptr) : QLabel(parent) {

        dtkFontAwesome::instance()->initFontAwesome();
        dtkFontAwesome::instance()->setDefaultOption("scale-factor", 1.0);
        dtkFontAwesome::instance()->setDefaultOption("color", QColor("#dddddd"));

        this->id = id;

        this->setPixmap(dtkFontAwesome::instance()->icon(fa::circlethin).pixmap(8, 8));
        this->setFixedWidth(8);
        this->setFixedHeight(8);

        if(!id)
            this->toggle(true);

        this->setMouseTracking(true);
    }

signals:
    void clicked(void);

public slots:
    void toggle(bool on) {
        this->setPixmap(dtkFontAwesome::instance()->icon(on ? fa::circle : fa::circlethin).pixmap(8, 8));
    }

protected:
    void mousePressEvent(QMouseEvent *) override {
        emit clicked();
    }

public:
    int id = -1;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsScrollerHUD : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsScrollerHUD(QWidget *parent = nullptr);
    ~dtkWidgetsScrollerHUD(void);

signals:
    void slide(int);

public:
    void increase(void);
    void decrease(void);
};

dtkWidgetsScrollerHUD::dtkWidgetsScrollerHUD(QWidget *parent) : QFrame(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setAlignment(Qt::AlignVCenter);

    this->setAttribute(Qt::WA_NoSystemBackground);
    this->setAttribute(Qt::WA_TranslucentBackground);
}

dtkWidgetsScrollerHUD::~dtkWidgetsScrollerHUD(void)
{

}

void dtkWidgetsScrollerHUD::increase(void)
{
    static int count = 0;

    dtkWidgetsScrollerHUDItem *item = new dtkWidgetsScrollerHUDItem(count++, this);

    connect(item, &dtkWidgetsScrollerHUDItem::clicked, [=] () {

        for(int i = 0; i < this->layout()->count(); i++)
            if(dtkWidgetsScrollerHUDItem *hud_item = dynamic_cast<dtkWidgetsScrollerHUDItem *>(this->layout()->itemAt(i)->widget()))
                hud_item->toggle(hud_item->id == item->id);

        emit slide(item->id);
    });

    this->layout()->addWidget(item);
}

void dtkWidgetsScrollerHUD::decrease(void)
{
    // TODO
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsScrollerPage : public QFrame
{
    Q_OBJECT

public:
    dtkWidgetsScrollerPage(const QString& text, QWidget *parent = nullptr);

public:
    QLabel *label;
};

dtkWidgetsScrollerPage::dtkWidgetsScrollerPage(const QString& text, QWidget *parent) : QFrame(parent)
{
    this->label = new QLabel(text, this);
    this->label->setAlignment(Qt::AlignCenter);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setAlignment(Qt::AlignCenter);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(this->label);

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsScroller : public QScrollArea
{
    Q_OBJECT

public:
    dtkWidgetsScroller(QWidget *parent = nullptr);

public:
    void    addPage(dtkWidgetsScrollerPage *);
    void removePage(dtkWidgetsScrollerPage *);

public slots:
    void scrollTo(int);

protected:
    void scroll(int dx);

protected:
    void resizeEvent(QResizeEvent *) override;
    void wheelEvent(QWheelEvent *) override;

public:
    QVBoxLayout *layout;

public:
    int from = 0;
};

dtkWidgetsScroller::dtkWidgetsScroller(QWidget *parent) : QScrollArea(parent)
{
    this->layout = new QVBoxLayout;
    this->layout->setContentsMargins(0, 0, 0, 0);
    this->layout->setSpacing(0);

    QWidget *contents = new QWidget(this);
    contents->setLayout(this->layout);

    this->setFrameShape(QFrame::NoFrame);
    this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    this->setWidgetResizable(true);
    this->setWidget(contents);
}

void dtkWidgetsScroller::addPage(dtkWidgetsScrollerPage *page)
{
    this->layout->addWidget(page);
}

void dtkWidgetsScroller::removePage(dtkWidgetsScrollerPage *page)
{
    this->layout->removeWidget(page);
}

void dtkWidgetsScroller::scrollTo(int to)
{
    if(this->layout->count() <= 1)
        return;

    if(to > this->layout->count())
        return;

    if(to < 0)
        return;

    double xf = this->from; this->from = to;
    double xt = to;

    double ss = this->verticalScrollBar()->value();
    double sm = this->verticalScrollBar()->maximum();
    double sc = this->layout->count() - 1;

    QVariantAnimation *animation = new QVariantAnimation;
    animation->setEasingCurve(QEasingCurve::OutCubic);
    animation->setStartValue(xf);
    animation->setEndValue(xt);
    animation->setDuration(500);

    connect(animation, &QVariantAnimation::valueChanged, [=] (const QVariant& value) {
        this->scroll(ss + (value.toDouble() - xf) * sm / sc);
    });

    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void dtkWidgetsScroller::scroll(int dx)
{
    this->verticalScrollBar()->setValue(dx);
}

void dtkWidgetsScroller::resizeEvent(QResizeEvent *event)
{
    for(int i = 0; i < this->layout->count(); i++)
        if(dtkWidgetsScrollerPage *page = dynamic_cast<dtkWidgetsScrollerPage *>(layout->itemAt(i)->widget()))
            page->setFixedHeight(event->size().height());

    this->repaint();
    this->scrollTo(this->from);

    QScrollArea::resizeEvent(event);
}

void dtkWidgetsScroller::wheelEvent(QWheelEvent *event)
{
    // TODO: Need to handle kinetic scrolling.

    // if (event->angleDelta().y() > 0)
    //     this->scrollTo(this->from-1);
    // else
    //     this->scrollTo(this->from+1);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummy : public QFrame
{
    Q_OBJECT

public:
    dummy(QWidget *parent = nullptr);

protected:
    void resizeEvent(QResizeEvent *);

private:
    dtkWidgetsScrollerHUD *hud;
    dtkWidgetsScroller *scroll;
};

dummy::dummy(QWidget *parent) : QFrame(parent)
{
    dtkWidgetsScrollerPage *page1 = new dtkWidgetsScrollerPage("Page 1", this);
    page1->setFixedHeight(480);
    page1->setStyleSheet("QFrame { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #26aae5, stop: 1.0 #1e5fa7); } QLabel { background: none; }");

    dtkWidgetsScrollerPage *page2 = new dtkWidgetsScrollerPage("Page 2", this);
    page2->setFixedHeight(480);
    page2->setStyleSheet("QFrame { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #4c37a0, stop: 1.0 #182a58); } QLabel { background: none; }");

    dtkWidgetsScrollerPage *page3 = new dtkWidgetsScrollerPage("Page 3", this);
    page3->setFixedHeight(480);
    page3->setStyleSheet("QFrame { background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #fe50b7, stop: 1.0 #f03a58); } QLabel { background: none; }");

    this->scroll = new dtkWidgetsScroller(this);
    this->scroll->setAlignment(Qt::AlignTop);
    this->scroll->addPage(page1);
    this->scroll->addPage(page2);
    this->scroll->addPage(page3);

    this->hud = new dtkWidgetsScrollerHUD(this);
    this->hud->increase();
    this->hud->increase();
    this->hud->increase();

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(this->scroll);

    connect(this->hud, &dtkWidgetsScrollerHUD::slide, [=] (int index) {
        this->scroll->scrollTo(index);
    });
}

void dummy::resizeEvent(QResizeEvent *event)
{
    static int size = 8;
    static int margin = 4;

    this->hud->move(event->size().width() - size - margin, 0);
    this->hud->resize(size, event->size().height());
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    QApplication application(argc, argv);

    dummy *dummy = new class dummy;
    dummy->setAttribute(Qt::WA_DeleteOnClose, true);
    dummy->resize(800, 480);
    dummy->show();
    dummy->raise();

    return application.exec();
}

// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
