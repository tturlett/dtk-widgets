// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include <Qt> // The long awaited ...

#include <dtkWidgets>
#include <dtkWidgetsExport>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#include <QtGui/private/qtguiglobal_p.h>
#include <QtGui/private/qdrawhelper_p.h>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

#define DTK_DECL_MEMROTATE(type)                            \
    void DTKWIDGETS_EXPORT dtk_memrotate90(const type*, int, int, int, type*, int); \
    void DTKWIDGETS_EXPORT dtk_memrotate180(const type*, int, int, int, type*, int); \
    void DTKWIDGETS_EXPORT dtk_memrotate270(const type*, int, int, int, type*, int)

DTK_DECL_MEMROTATE(quint32);
DTK_DECL_MEMROTATE(quint16);
DTK_DECL_MEMROTATE(quint24);
DTK_DECL_MEMROTATE(quint8);
DTK_DECL_MEMROTATE(quint64);

#undef DTK_DECL_MEMROTATE

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

static const int tileSize = 32;

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate90_tiled(const T *src, int w, int h, int sstride, T *dest, int dstride)
{
    sstride /= sizeof(T);
    dstride /= sizeof(T);

    const int pack = sizeof(quint32) / sizeof(T);
    const int unaligned = qMin(uint((quintptr(dest) & (sizeof(quint32)-1)) / sizeof(T)), uint(h));
    const int restX = w % tileSize;
    const int restY = (h - unaligned) % tileSize;
    const int unoptimizedY = restY % pack;
    const int numTilesX = w / tileSize + (restX > 0);
    const int numTilesY = (h - unaligned) / tileSize + (restY >= pack);

    for (int tx = 0; tx < numTilesX; ++tx) {
        const int startx = w - tx * tileSize - 1;
        const int stopx = qMax(startx - tileSize, 0);

        if (unaligned) {
            for (int x = startx; x >= stopx; --x) {
                T *d = dest + (w - x - 1) * dstride;
                for (int y = 0; y < unaligned; ++y) {
                    *d++ = src[y * sstride + x];
                }
            }
        }

        for (int ty = 0; ty < numTilesY; ++ty) {
            const int starty = ty * tileSize + unaligned;
            const int stopy = qMin(starty + tileSize, h - unoptimizedY);

            for (int x = startx; x >= stopx; --x) {
                quint32 *d = reinterpret_cast<quint32*>(dest + (w - x - 1) * dstride + starty);
                for (int y = starty; y < stopy; y += pack) {
                    quint32 c = src[y * sstride + x];
                    for (int i = 1; i < pack; ++i) {
                        const int shift = (sizeof(T) * 8 * i);
                        const T color = src[(y + i) * sstride + x];
                        c |= color << shift;
                    }
                    *d++ = c;
                }
            }
        }

        if (unoptimizedY) {
            const int starty = h - unoptimizedY;
            for (int x = startx; x >= stopx; --x) {
                T *d = dest + (w - x - 1) * dstride + starty;
                for (int y = starty; y < h; ++y) {
                    *d++ = src[y * sstride + x];
                }
            }
        }
    }
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate90_tiled_unpacked(const T *src, int w, int h, int sstride, T *dest, int dstride)
{
    const int numTilesX = (w + tileSize - 1) / tileSize;
    const int numTilesY = (h + tileSize - 1) / tileSize;

    for (int tx = 0; tx < numTilesX; ++tx) {
        const int startx = w - tx * tileSize - 1;
        const int stopx = qMax(startx - tileSize, 0);

        for (int ty = 0; ty < numTilesY; ++ty) {
            const int starty = ty * tileSize;
            const int stopy = qMin(starty + tileSize, h);

            for (int x = startx; x >= stopx; --x) {
                T *d = (T *)((char*)dest + (w - x - 1) * dstride) + starty;
                const char *s = (const char*)(src + x) + starty * sstride;
                for (int y = starty; y < stopy; ++y) {
                    *d++ = *(const T *)(s);
                    s += sstride;
                }
            }
        }
    }
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate270_tiled(const T *src, int w, int h, int sstride, T *dest, int dstride)
{
    sstride /= sizeof(T);
    dstride /= sizeof(T);

    const int pack = sizeof(quint32) / sizeof(T);
    const int unaligned = qMin(uint((quintptr(dest) & (sizeof(quint32)-1)) / sizeof(T)), uint(h));
    const int restX = w % tileSize;
    const int restY = (h - unaligned) % tileSize;
    const int unoptimizedY = restY % pack;
    const int numTilesX = w / tileSize + (restX > 0);
    const int numTilesY = (h - unaligned) / tileSize + (restY >= pack);

    for (int tx = 0; tx < numTilesX; ++tx) {
        const int startx = tx * tileSize;
        const int stopx = qMin(startx + tileSize, w);

        if (unaligned) {
            for (int x = startx; x < stopx; ++x) {
                T *d = dest + x * dstride;
                for (int y = h - 1; y >= h - unaligned; --y) {
                    *d++ = src[y * sstride + x];
                }
            }
        }

        for (int ty = 0; ty < numTilesY; ++ty) {
            const int starty = h - 1 - unaligned - ty * tileSize;
            const int stopy = qMax(starty - tileSize, unoptimizedY);

            for (int x = startx; x < stopx; ++x) {
                quint32 *d = reinterpret_cast<quint32*>(dest + x * dstride
                                                        + h - 1 - starty);
                for (int y = starty; y >= stopy; y -= pack) {
                    quint32 c = src[y * sstride + x];
                    for (int i = 1; i < pack; ++i) {
                        const int shift = (sizeof(T) * 8 * i);
                        const T color = src[(y - i) * sstride + x];
                        c |= color << shift;
                    }
                    *d++ = c;
                }
            }
        }
        if (unoptimizedY) {
            const int starty = unoptimizedY - 1;
            for (int x = startx; x < stopx; ++x) {
                T *d = dest + x * dstride + h - 1 - starty;
                for (int y = starty; y >= 0; --y) {
                    *d++ = src[y * sstride + x];
                }
            }
        }
    }
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate270_tiled_unpacked(const T *src, int w, int h, int sstride, T *dest, int dstride)
{
    const int numTilesX = (w + tileSize - 1) / tileSize;
    const int numTilesY = (h + tileSize - 1) / tileSize;

    for (int tx = 0; tx < numTilesX; ++tx) {
        const int startx = tx * tileSize;
        const int stopx = qMin(startx + tileSize, w);

        for (int ty = 0; ty < numTilesY; ++ty) {
            const int starty = h - 1 - ty * tileSize;
            const int stopy = qMax(starty - tileSize, 0);

            for (int x = startx; x < stopx; ++x) {
                T *d = (T*)((char*)dest + x * dstride) + h - 1 - starty;
                const char *s = (const char*)(src + x) + starty * sstride;
                for (int y = starty; y >= stopy; --y) {
                    *d++ = *(const T*)s;
                    s -= sstride;
                }
            }
        }
    }
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate90_template(const T *src, int srcWidth, int srcHeight, int srcStride, T *dest, int dstStride)
{
#if Q_BYTE_ORDER == Q_LITTLE_ENDIAN
    if (sizeof(quint32) % sizeof(T) == 0)
        dtk_memrotate90_tiled<T>(src, srcWidth, srcHeight, srcStride, dest, dstStride);
    else
#endif
    dtk_memrotate90_tiled_unpacked<T>(src, srcWidth, srcHeight, srcStride, dest, dstStride);
}

template <> inline void dtk_memrotate90_template<quint32>(const quint32 *src, int w, int h, int sstride, quint32 *dest, int dstride)
{
    dtk_memrotate90_tiled_unpacked(src, w, h, sstride, dest, dstride);
}

template <> inline void dtk_memrotate90_template<quint64>(const quint64 *src, int w, int h, int sstride, quint64 *dest, int dstride)
{
    dtk_memrotate90_tiled_unpacked(src, w, h, sstride, dest, dstride);
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate180_template(const T *src, int w, int h, int sstride, T *dest, int dstride)
{
    const char *s = (const char*)(src) + (h - 1) * sstride;

    for (int dy = 0; dy < h; ++dy) {
        T *d = reinterpret_cast<T*>((char *)(dest) + dy * dstride);
        src = reinterpret_cast<const T*>(s);
        for (int dx = 0; dx < w; ++dx) {
            d[dx] = src[w - 1 - dx];
        }
        s -= sstride;
    }
}

template <class T> Q_STATIC_TEMPLATE_FUNCTION inline void dtk_memrotate270_template(const T *src, int srcWidth, int srcHeight, int srcStride, T *dest, int dstStride)
{
#if Q_BYTE_ORDER == Q_LITTLE_ENDIAN
    if (sizeof(quint32) % sizeof(T) == 0)
        dtk_memrotate270_tiled<T>(src, srcWidth, srcHeight, srcStride, dest, dstStride);
    else
#endif
    dtk_memrotate270_tiled_unpacked<T>(src, srcWidth, srcHeight, srcStride, dest, dstStride);
}

template <> inline void dtk_memrotate270_template<quint32>(const quint32 *src, int w, int h, int sstride, quint32 *dest, int dstride)
{
    dtk_memrotate270_tiled_unpacked(src, w, h, sstride, dest, dstride);
}

template <> inline void dtk_memrotate270_template<quint64>(const quint64 *src, int w, int h, int sstride, quint64 *dest, int dstride)
{
    dtk_memrotate270_tiled_unpacked(src, w, h, sstride, dest, dstride);
}

#define DTK_IMPL_MEMROTATE(type) DTKWIDGETS_EXPORT void dtk_memrotate90(const type *src, int w, int h, int sstride, type *dest, int dstride) \
{                                                                   \
    dtk_memrotate90_template(src, w, h, sstride, dest, dstride);        \
}                                                                       \
DTKWIDGETS_EXPORT void dtk_memrotate180(const type *src, int w, int h, int sstride, type *dest, int dstride)          \
{                                                                       \
    dtk_memrotate180_template(src, w, h, sstride, dest, dstride);       \
}                                                                       \
DTKWIDGETS_EXPORT void dtk_memrotate270(const type *src, int w, int h, int sstride, type *dest, int dstride)          \
{                                                                       \
    dtk_memrotate270_template(src, w, h, sstride, dest, dstride);       \
}

#define DTK_IMPL_SIMPLE_MEMROTATE(type)                                 \
DTKWIDGETS_EXPORT void dtk_memrotate90(const type *src, int w, int h, int sstride, type *dest, int dstride)           \
{                                                                   \
    dtk_memrotate90_tiled_unpacked(src, w, h, sstride, dest, dstride); \
}                                                                   \
DTKWIDGETS_EXPORT void dtk_memrotate180(const type *src, int w, int h, int sstride, type *dest, int dstride)          \
{                                                                   \
    dtk_memrotate180_template(src, w, h, sstride, dest, dstride);    \
}                                                                   \
DTKWIDGETS_EXPORT void dtk_memrotate270(const type *src, int w, int h, int sstride, type *dest, int dstride)          \
{                                                                   \
    dtk_memrotate270_tiled_unpacked(src, w, h, sstride, dest, dstride); \
}

DTK_IMPL_MEMROTATE(quint64)
DTK_IMPL_MEMROTATE(quint32)
DTK_IMPL_MEMROTATE(quint16)
DTK_IMPL_MEMROTATE(quint24)
DTK_IMPL_MEMROTATE(quint8)

void dtk_memrotate90_8(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate90(srcPixels, w, h, sbpl, destPixels, dbpl);
}

void dtk_memrotate180_8(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate180(srcPixels, w, h, sbpl, destPixels, dbpl);
}

void dtk_memrotate270_8(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate270(srcPixels, w, h, sbpl, destPixels, dbpl);
}

void dtk_memrotate90_16(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate90((const ushort *)srcPixels, w, h, sbpl, (ushort *)destPixels, dbpl);
}

void dtk_memrotate180_16(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate180((const ushort *)srcPixels, w, h, sbpl, (ushort *)destPixels, dbpl);
}

void dtk_memrotate270_16(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate270((const ushort *)srcPixels, w, h, sbpl, (ushort *)destPixels, dbpl);
}

void dtk_memrotate90_24(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate90((const quint24 *)srcPixels, w, h, sbpl, (quint24 *)destPixels, dbpl);
}

void dtk_memrotate180_24(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate180((const quint24 *)srcPixels, w, h, sbpl, (quint24 *)destPixels, dbpl);
}

void dtk_memrotate270_24(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate270((const quint24 *)srcPixels, w, h, sbpl, (quint24 *)destPixels, dbpl);
}

void dtk_memrotate90_32(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate90((const uint *)srcPixels, w, h, sbpl, (uint *)destPixels, dbpl);
}

void dtk_memrotate180_32(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate180((const uint *)srcPixels, w, h, sbpl, (uint *)destPixels, dbpl);
}

void dtk_memrotate270_32(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate270((const uint *)srcPixels, w, h, sbpl, (uint *)destPixels, dbpl);
}


void dtk_memrotate90_64(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate90((const quint64 *)srcPixels, w, h, sbpl, (quint64 *)destPixels, dbpl);
}

void dtk_memrotate180_64(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate180((const quint64 *)srcPixels, w, h, sbpl, (quint64 *)destPixels, dbpl);
}

void dtk_memrotate270_64(const uchar *srcPixels, int w, int h, int sbpl, uchar *destPixels, int dbpl)
{
    dtk_memrotate270((const quint64 *)srcPixels, w, h, sbpl, (quint64 *)destPixels, dbpl);
}

MemRotateFunc qMemRotateFunctions[QPixelLayout::BPPCount][3] = {
    { 0, 0, 0 },
    { 0, 0, 0 },
    { 0, 0, 0 },
    { dtk_memrotate90_8,  dtk_memrotate180_8,  dtk_memrotate270_8  },
    { dtk_memrotate90_16, dtk_memrotate180_16, dtk_memrotate270_16 },
    { dtk_memrotate90_24, dtk_memrotate180_24, dtk_memrotate270_24 },
    { dtk_memrotate90_32, dtk_memrotate180_32, dtk_memrotate270_32 },
    { dtk_memrotate90_64, dtk_memrotate180_64, dtk_memrotate270_64 },
};

// ///////////////////////////////////////////////////////////////////
// NOTE: Shameless fork of Qt's internals                           //
// ///////////////////////////////////////////////////////////////////

class dtkPixmapFilter : public QObject
{
    Q_OBJECT

public:
    virtual ~dtkPixmapFilter(void) = 0;

    enum FilterType {
        ConvolutionFilter,
        ColorizeFilter,
        DropShadowFilter,
        BlurFilter,
        UserFilter = 1024
    };

    FilterType type(void) const;

    virtual QRectF boundingRectFor(const QRectF &rect) const;

    virtual void draw(QPainter *painter, const QPointF &p, const QPixmap &src, const QRectF &srcRect = QRectF()) const = 0;

private:
    class dtkPixmapFilterPrivate *d;

protected:
    dtkPixmapFilter(dtkPixmapFilterPrivate &d, FilterType type, QObject *parent);
    dtkPixmapFilter(FilterType type, QObject *parent);
};

// ///////////////////////////////////////////////////////////////////
// Wrappers
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsEffectPrivate;
class dtkGraphicsEffectSource;

class DTKWIDGETS_EXPORT dtkGraphicsEffect : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)

public:
    enum ChangeFlag {
        SourceAttached = 0x1,
        SourceDetached = 0x2,
        SourceBoundingRectChanged = 0x4,
        SourceInvalidated = 0x8
    };
    Q_DECLARE_FLAGS(ChangeFlags, ChangeFlag);
    Q_FLAG(ChangeFlags);

public:
    enum PixmapPadMode {
        NoPad,
        PadToTransparentBorder,
        PadToEffectiveBoundingRect
    };

public:
             dtkGraphicsEffect(QObject *parent = nullptr);
    virtual ~dtkGraphicsEffect(void);

public:
    void setGraphicsEffectSource(dtkGraphicsEffectSource *newSource);

public:
    virtual QRectF boundingRectFor(const QRectF &sourceRect) const;
    QRectF boundingRect(void) const;

    bool isEnabled(void) const;

public Q_SLOTS:
    void setEnabled(bool enable);
    void update(void);

Q_SIGNALS:
    void enabledChanged(bool enabled);

protected:
    dtkGraphicsEffect(dtkGraphicsEffectPrivate &d, QObject *parent = nullptr);
    virtual void draw(QPainter *painter) = 0;
    virtual void sourceChanged(ChangeFlags flags);
    void updateBoundingRect(void);

    bool sourceIsPixmap(void) const;
    QRectF sourceBoundingRect(Qt::CoordinateSystem system = Qt::LogicalCoordinates) const;
    void drawSource(QPainter *painter);
    QPixmap sourcePixmap(Qt::CoordinateSystem system = Qt::LogicalCoordinates, QPoint *offset = nullptr, PixmapPadMode mode = PadToEffectiveBoundingRect) const;

private:
    Q_DISABLE_COPY(dtkGraphicsEffect)

private:
    dtkGraphicsEffectPrivate *d;

public:
    dtkGraphicsEffectSource *source(void) const;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(dtkGraphicsEffect::ChangeFlags)

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapConvolutionFilter : public dtkPixmapFilter
{
    Q_OBJECT

public:
     dtkPixmapConvolutionFilter(QObject *parent = 0);
    ~dtkPixmapConvolutionFilter(void);

    void setConvolutionKernel(const qreal *matrix, int rows, int columns);

    QRectF boundingRectFor(const QRectF &rect) const override;
    void draw(QPainter *painter, const QPointF &dest, const QPixmap &src, const QRectF &srcRect = QRectF()) const override;

private:
    const qreal *convolutionKernel(void) const;
    int rows() const;
    int columns(void) const;

private:
    class dtkPixmapConvolutionFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsBlurEffectPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkGraphicsBlurEffect: public dtkGraphicsEffect
{
    Q_OBJECT
    Q_PROPERTY(qreal blurRadius READ blurRadius WRITE setBlurRadius NOTIFY blurRadiusChanged)
    Q_PROPERTY(BlurHints blurHints READ blurHints WRITE setBlurHints NOTIFY blurHintsChanged)

public:
    enum BlurHint {
        PerformanceHint = 0x00,
        QualityHint = 0x01,
        AnimationHint = 0x02
    };
    Q_FLAG(BlurHint);
    Q_DECLARE_FLAGS(BlurHints, BlurHint);
    Q_FLAG(BlurHints);

public:
     dtkGraphicsBlurEffect(QObject *parent = nullptr);
    ~dtkGraphicsBlurEffect(void);

public:
    QRectF boundingRectFor(const QRectF &rect) const override;
    qreal blurRadius(void) const;
    BlurHints blurHints(void) const;

public slots:
    void setBlurRadius(qreal blurRadius);
    void setBlurHints(BlurHints hints);

signals:
    void blurRadiusChanged(qreal blurRadius);
    void blurHintsChanged(BlurHints hints);

// ///////////////////////////////////////////////////////////////////
// TODO:
// ///////////////////////////////////////////////////////////////////
// Was:
// ///////////////////////////////////////////////////////////////////
// protected:
// ///////////////////////////////////////////////////////////////////
public:
// ///////////////////////////////////////////////////////////////////
    void draw(QPainter *painter) override;
// ///////////////////////////////////////////////////////////////////

private:
    Q_DISABLE_COPY(dtkGraphicsBlurEffect);

private:
    dtkGraphicsBlurEffectPrivate *d;
};

Q_DECLARE_OPERATORS_FOR_FLAGS(dtkGraphicsBlurEffect::BlurHints)

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapBlurFilter : public dtkPixmapFilter
{
    Q_OBJECT

public:
     dtkPixmapBlurFilter(QObject *parent = 0);
    ~dtkPixmapBlurFilter(void);

public:
    void setRadius(qreal radius);
    void setBlurHints(dtkGraphicsBlurEffect::BlurHints hints);

    qreal radius(void) const;

public:
    dtkGraphicsBlurEffect::BlurHints blurHints(void) const;

public:
    QRectF boundingRectFor(const QRectF &rect) const override;

public:
    void draw(QPainter *painter, const QPointF &dest, const QPixmap &src, const QRectF &srcRect = QRectF()) const override;

private:
    class dtkPixmapBlurFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapColorizeFilter : public dtkPixmapFilter
{
    Q_OBJECT

public:
     dtkPixmapColorizeFilter(QObject *parent = 0);
    ~dtkPixmapColorizeFilter(void);

public:
    void setColor(const QColor& color);
    QColor color(void) const;

public:
    void setStrength(qreal strength);
    qreal strength(void) const;

public:
    void draw(QPainter *painter, const QPointF &dest, const QPixmap &src, const QRectF &srcRect = QRectF()) const override;

private:
    class dtkPixmapColorizeFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapDropShadowFilter : public dtkPixmapFilter
{
    Q_OBJECT

public:
     dtkPixmapDropShadowFilter(QObject *parent = 0);
    ~dtkPixmapDropShadowFilter(void);

public:
    QRectF boundingRectFor(const QRectF &rect) const override;

public:
    void draw(QPainter *p, const QPointF &pos, const QPixmap &px, const QRectF &src = QRectF()) const override;

public:
    qreal blurRadius(void) const;
    void setBlurRadius(qreal radius);

public:
    QColor color(void) const;
    void setColor(const QColor &color);

public:
    QPointF offset(void) const;
    void setOffset(const QPointF &offset);
    inline void setOffset(qreal dx, qreal dy) { setOffset(QPointF(dx, dy)); }

private:
    class dtkPixmapDropShadowFilterPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapFilterPrivate
{
public:
    dtkPixmapFilterPrivate(void) {}

public:
    dtkPixmapFilter::FilterType type;

public:
    dtkPixmapFilter *q = nullptr;
};

dtkPixmapFilter::dtkPixmapFilter(FilterType type, QObject *parent) : QObject(parent)
{
    d = new dtkPixmapFilterPrivate;
    d->type = type;
}

dtkPixmapFilter::dtkPixmapFilter(dtkPixmapFilterPrivate& d, dtkPixmapFilter::FilterType type, QObject *parent) : QObject(parent)
{
    this->d = &d;
    this->d->type = type;
}

dtkPixmapFilter::~dtkPixmapFilter(void)
{

}

dtkPixmapFilter::FilterType dtkPixmapFilter::type(void) const
{
    return d->type;
}

QRectF dtkPixmapFilter::boundingRectFor(const QRectF &rect) const
{
    return rect;
}

class dtkPixmapConvolutionFilterPrivate : public dtkPixmapFilterPrivate
{
public:
     dtkPixmapConvolutionFilterPrivate(void) : convolutionKernel(0), kernelWidth(0), kernelHeight(0), convoluteAlpha(false) {}
    ~dtkPixmapConvolutionFilterPrivate(void) {
        delete[] convolutionKernel;
    }

public:
    qreal *convolutionKernel;

public:
    int kernelWidth;
    int kernelHeight;

public:
    bool convoluteAlpha;
};

dtkPixmapConvolutionFilter::dtkPixmapConvolutionFilter(QObject *parent) : dtkPixmapFilter(*new dtkPixmapConvolutionFilterPrivate, ConvolutionFilter, parent)
{
    d->convoluteAlpha = true;
}

dtkPixmapConvolutionFilter::~dtkPixmapConvolutionFilter(void)
{

}

void dtkPixmapConvolutionFilter::setConvolutionKernel(const qreal *kernel, int rows, int columns)
{
    delete [] d->convolutionKernel;
    d->convolutionKernel = new qreal[rows * columns];
    memcpy(d->convolutionKernel, kernel, sizeof(qreal) * rows * columns);
    d->kernelWidth = columns;
    d->kernelHeight = rows;
}

const qreal *dtkPixmapConvolutionFilter::convolutionKernel(void) const
{
    return d->convolutionKernel;
}

int dtkPixmapConvolutionFilter::rows(void) const
{
    return d->kernelHeight;
}

int dtkPixmapConvolutionFilter::columns(void) const
{
    return d->kernelWidth;
}

QRectF dtkPixmapConvolutionFilter::boundingRectFor(const QRectF &rect) const
{
    return rect.adjusted(-d->kernelWidth / 2, -d->kernelHeight / 2, (d->kernelWidth - 1) / 2, (d->kernelHeight - 1) / 2);
}

static void convolute(QImage *destImage, const QPointF &pos, const QImage &srcImage, const QRectF &srcRect, QPainter::CompositionMode mode, qreal *kernel, int kernelWidth, int kernelHeight )
{
    const QImage processImage = (srcImage.format() != QImage::Format_ARGB32_Premultiplied ) ?               srcImage.convertToFormat(QImage::Format_ARGB32_Premultiplied) : srcImage;

    int *fixedKernel = new int[kernelWidth*kernelHeight];

    for(int i = 0; i < kernelWidth*kernelHeight; i++)
    {
        fixedKernel[i] = (int)(65536 * kernel[i]);
    }
    QRectF trect = srcRect.isNull() ? processImage.rect() : srcRect;
    trect.moveTo(pos);
    QRectF bounded = trect.adjusted(-kernelWidth / 2, -kernelHeight / 2, (kernelWidth - 1) / 2, (kernelHeight - 1) / 2);
    QRect rect = bounded.toAlignedRect();
    QRect targetRect = rect.intersected(destImage->rect());

    QRectF srect = srcRect.isNull() ? processImage.rect() : srcRect;
    QRectF sbounded = srect.adjusted(-kernelWidth / 2, -kernelHeight / 2, (kernelWidth - 1) / 2, (kernelHeight - 1) / 2);
    QPoint srcStartPoint = sbounded.toAlignedRect().topLeft()+(targetRect.topLeft()-rect.topLeft());

    const uint *sourceStart = (const uint*)processImage.scanLine(0);
    uint *outputStart = (uint*)destImage->scanLine(0);

    int yk = srcStartPoint.y();
    for (int y = targetRect.top(); y <= targetRect.bottom(); y++) {
        uint* output = outputStart + (destImage->bytesPerLine()/sizeof(uint))*y+targetRect.left();
        int xk = srcStartPoint.x();
        for(int x = targetRect.left(); x <= targetRect.right(); x++) {
            int r = 0;
            int g = 0;
            int b = 0;
            int a = 0;

            int kernely = -kernelHeight/2;
            int starty = 0;
            int endy = kernelHeight;
            if(yk+kernely+endy >= srcImage.height())
                endy = kernelHeight-((yk+kernely+endy)-srcImage.height())-1;
            if(yk+kernely < 0)
                starty = -(yk+kernely);

            int kernelx = -kernelWidth/2;
            int startx = 0;
            int endx = kernelWidth;
            if(xk+kernelx+endx >= srcImage.width())
                endx = kernelWidth-((xk+kernelx+endx)-srcImage.width())-1;
            if(xk+kernelx < 0)
                startx = -(xk+kernelx);

            for (int ys = starty; ys < endy; ys ++) {
                const uint *pix = sourceStart + (processImage.bytesPerLine()/sizeof(uint))*(yk+kernely+ys) + ((xk+kernelx+startx));
                const uint *endPix = pix+endx-startx;
                int kernelPos = ys*kernelWidth+startx;
                while (pix < endPix) {
                    int factor = fixedKernel[kernelPos++];
                    a += (((*pix) & 0xff000000)>>24) * factor;
                    r += (((*pix) & 0x00ff0000)>>16) * factor;
                    g += (((*pix) & 0x0000ff00)>>8 ) * factor;
                    b += (((*pix) & 0x000000ff)    ) * factor;
                    pix++;
                }
            }

            r = qBound((int)0, r >> 16, (int)255);
            g = qBound((int)0, g >> 16, (int)255);
            b = qBound((int)0, b >> 16, (int)255);
            a = qBound((int)0, a >> 16, (int)255);

            if(mode == QPainter::CompositionMode_Source) {
                uint color = (a<<24)+(r<<16)+(g<<8)+b;
                *output++ = color;
            } else {
                uint current = *output;
                uchar ca = (current&0xff000000)>>24;
                uchar cr = (current&0x00ff0000)>>16;
                uchar cg = (current&0x0000ff00)>>8;
                uchar cb = (current&0x000000ff);
                uint color =
                        (((ca*(255-a) >> 8)+a) << 24)+
                        (((cr*(255-a) >> 8)+r) << 16)+
                        (((cg*(255-a) >> 8)+g) << 8)+
                        (((cb*(255-a) >> 8)+b));
                *output++ = color;;
            }
            xk++;
        }
        yk++;
    }
    delete[] fixedKernel;
}

void dtkPixmapConvolutionFilter::draw(QPainter *painter, const QPointF &p, const QPixmap &src, const QRectF& srcRect) const
{
    if (!painter->isActive())
        return;

    if(d->kernelWidth<=0 || d->kernelHeight <= 0)
        return;

    if (src.isNull())
        return;

    QImage *target = 0;

    if (painter->paintEngine()->paintDevice()->devType() == QInternal::Image) {
        target = static_cast<QImage *>(painter->paintEngine()->paintDevice());

        QTransform mat = painter->combinedTransform();

        if (mat.type() > QTransform::TxTranslate) {
            target = 0;
        }
    }

    if (target) {
        QTransform x = painter->deviceTransform();
        QPointF offset(x.dx(), x.dy());

        convolute(target, p+offset, src.toImage(), srcRect, QPainter::CompositionMode_SourceOver, d->convolutionKernel, d->kernelWidth, d->kernelHeight);
    } else {
        QRect srect = srcRect.isNull() ? src.rect() : srcRect.toRect();
        QRect rect = boundingRectFor(srect).toRect();
        QImage result = QImage(rect.size(), QImage::Format_ARGB32_Premultiplied);
        QPoint offset = srect.topLeft() - rect.topLeft();

        convolute(&result, offset, src.toImage(), srect, QPainter::CompositionMode_Source, d->convolutionKernel, d->kernelWidth, d->kernelHeight);

        painter->drawImage(p - offset, result);
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapBlurFilterPrivate : public dtkPixmapFilterPrivate
{
public:
    dtkPixmapBlurFilterPrivate(void) : radius(5), hints(dtkGraphicsBlurEffect::PerformanceHint) {}

public:
    qreal radius;

public:
    dtkGraphicsBlurEffect::BlurHints hints;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkPixmapBlurFilter::dtkPixmapBlurFilter(QObject *parent) : dtkPixmapFilter(BlurFilter, parent)
{
    d = new dtkPixmapBlurFilterPrivate;
}

dtkPixmapBlurFilter::~dtkPixmapBlurFilter(void)
{
    delete d;
}

void dtkPixmapBlurFilter::setRadius(qreal radius)
{
    d->radius = radius;
}

qreal dtkPixmapBlurFilter::radius(void) const
{
    return d->radius;
}

void dtkPixmapBlurFilter::setBlurHints(dtkGraphicsBlurEffect::BlurHints hints)
{
    qDebug() << Q_FUNC_INFO << hints;

    d->hints = hints;
}

dtkGraphicsBlurEffect::BlurHints dtkPixmapBlurFilter::blurHints(void) const
{
    return d->hints;
}

const qreal radiusScale = qreal(2.5);

QRectF dtkPixmapBlurFilter::boundingRectFor(const QRectF &rect) const
{
    const qreal delta = radiusScale * d->radius + 1;

    return rect.adjusted(-delta, -delta, delta, delta);
}

template <int shift> inline int dtk_static_shift(int value)
{
    if (shift == 0)
        return value;
    else if (shift > 0)
        return value << (uint(shift) & 0x1f);
    else
        return value >> (uint(-shift) & 0x1f);
}

template<int aprec, int zprec> inline void dtk_blurinner(uchar *bptr, int &zR, int &zG, int &zB, int &zA, int alpha)
{
    QRgb *pixel = (QRgb *)bptr;

#define Z_MASK (0xff << zprec)
    const int A_zprec = dtk_static_shift<zprec - 24>(*pixel) & Z_MASK;
    const int R_zprec = dtk_static_shift<zprec - 16>(*pixel) & Z_MASK;
    const int G_zprec = dtk_static_shift<zprec - 8>(*pixel)  & Z_MASK;
    const int B_zprec = dtk_static_shift<zprec>(*pixel)      & Z_MASK;
#undef Z_MASK

    const int zR_zprec = zR >> aprec;
    const int zG_zprec = zG >> aprec;
    const int zB_zprec = zB >> aprec;
    const int zA_zprec = zA >> aprec;

    zR += alpha * (R_zprec - zR_zprec);
    zG += alpha * (G_zprec - zG_zprec);
    zB += alpha * (B_zprec - zB_zprec);
    zA += alpha * (A_zprec - zA_zprec);

#define ZA_MASK (0xff << (zprec + aprec))
    *pixel =
        dtk_static_shift<24 - zprec - aprec>(zA & ZA_MASK)
        | dtk_static_shift<16 - zprec - aprec>(zR & ZA_MASK)
        | dtk_static_shift<8 - zprec - aprec>(zG & ZA_MASK)
        | dtk_static_shift<-zprec - aprec>(zB & ZA_MASK);
#undef ZA_MASK
}

const int alphaIndex = (QSysInfo::ByteOrder == QSysInfo::BigEndian ? 0 : 3);

template<int aprec, int zprec> inline void dtk_blurinner_alphaOnly(uchar *bptr, int &z, int alpha)
{
    const int A_zprec = int(*(bptr)) << zprec;
    const int z_zprec = z >> aprec;
    z += alpha * (A_zprec - z_zprec);
    *(bptr) = z >> (zprec + aprec);
}

template<int aprec, int zprec, bool alphaOnly> inline void dtk_blurrow(QImage & im, int line, int alpha)
{
    uchar *bptr = im.scanLine(line);

    int zR = 0, zG = 0, zB = 0, zA = 0;

    if (alphaOnly && im.format() != QImage::Format_Indexed8)
        bptr += alphaIndex;

    const int stride = im.depth() >> 3;
    const int im_width = im.width();
    for (int index = 0; index < im_width; ++index) {
        if (alphaOnly)
            dtk_blurinner_alphaOnly<aprec, zprec>(bptr, zA, alpha);
        else
            dtk_blurinner<aprec, zprec>(bptr, zR, zG, zB, zA, alpha);
        bptr += stride;
    }

    bptr -= stride;

    for (int index = im_width - 2; index >= 0; --index) {
        bptr -= stride;
        if (alphaOnly)
            dtk_blurinner_alphaOnly<aprec, zprec>(bptr, zA, alpha);
        else
            dtk_blurinner<aprec, zprec>(bptr, zR, zG, zB, zA, alpha);
    }
}

template <int aprec, int zprec, bool alphaOnly> void expblur(QImage &img, qreal radius, bool improvedQuality = false, int transposed = 0)
{
    if (improvedQuality)
        radius *= qreal(0.5);

    Q_ASSERT(img.format() == QImage::Format_ARGB32_Premultiplied
             || img.format() == QImage::Format_RGB32
             || img.format() == QImage::Format_Indexed8
             || img.format() == QImage::Format_Grayscale8);

    const qreal cutOffIntensity = 2;

    int alpha = radius <= qreal(1e-5)
        ? ((1 << aprec)-1)
        : qRound((1<<aprec)*(1 - qPow(cutOffIntensity * (1 / qreal(255)), 1 / radius)));

    int img_height = img.height();

    for (int row = 0; row < img_height; ++row) {
        for (int i = 0; i <= int(improvedQuality); ++i)
            dtk_blurrow<aprec, zprec, alphaOnly>(img, row, alpha);
    }

    QImage temp(img.height(), img.width(), img.format());
    temp.setDevicePixelRatio(img.devicePixelRatioF());

    if (transposed >= 0) {
        if (img.depth() == 8) {
            dtk_memrotate270(reinterpret_cast<const quint8*>(img.bits()),
                            img.width(), img.height(), img.bytesPerLine(),
                            reinterpret_cast<quint8*>(temp.bits()),
                            temp.bytesPerLine());
        } else {
            dtk_memrotate270(reinterpret_cast<const quint32*>(img.bits()),
                            img.width(), img.height(), img.bytesPerLine(),
                            reinterpret_cast<quint32*>(temp.bits()),
                            temp.bytesPerLine());
        }
    } else {
        if (img.depth() == 8) {
            dtk_memrotate90(reinterpret_cast<const quint8*>(img.bits()),
                           img.width(), img.height(), img.bytesPerLine(),
                           reinterpret_cast<quint8*>(temp.bits()),
                           temp.bytesPerLine());
        } else {
            dtk_memrotate90(reinterpret_cast<const quint32*>(img.bits()),
                           img.width(), img.height(), img.bytesPerLine(),
                           reinterpret_cast<quint32*>(temp.bits()),
                           temp.bytesPerLine());
        }
    }

    img_height = temp.height();

    for (int row = 0; row < img_height; ++row) {
        for (int i = 0; i <= int(improvedQuality); ++i)
            dtk_blurrow<aprec, zprec, alphaOnly>(temp, row, alpha);
    }

    if (transposed == 0) {
        if (img.depth() == 8) {
            dtk_memrotate90(reinterpret_cast<const quint8*>(temp.bits()),
                           temp.width(), temp.height(), temp.bytesPerLine(),
                           reinterpret_cast<quint8*>(img.bits()),
                           img.bytesPerLine());
        } else {
            dtk_memrotate90(reinterpret_cast<const quint32*>(temp.bits()),
                           temp.width(), temp.height(), temp.bytesPerLine(),
                           reinterpret_cast<quint32*>(img.bits()),
                           img.bytesPerLine());
        }
    } else {
        img = temp;
    }
}

#define AVG(a,b)  ( ((((a)^(b)) & 0xfefefefeUL) >> 1) + ((a)&(b)) )
#define AVG16(a,b)  ( ((((a)^(b)) & 0xf7deUL) >> 1) + ((a)&(b)) )

QImage dtk_halfScaled(const QImage &source)
{
    if (source.width() < 2 || source.height() < 2)
        return QImage();

    QImage srcImage = source;

    if (source.format() == QImage::Format_Indexed8 || source.format() == QImage::Format_Grayscale8) {

        QImage dest(source.width() / 2, source.height() / 2, srcImage.format());
        dest.setDevicePixelRatio(source.devicePixelRatioF());

        const uchar *src = reinterpret_cast<const uchar*>(const_cast<const QImage &>(srcImage).bits());
        int sx = srcImage.bytesPerLine();
        int sx2 = sx << 1;

        uchar *dst = reinterpret_cast<uchar*>(dest.bits());
        int dx = dest.bytesPerLine();
        int ww = dest.width();
        int hh = dest.height();

        for (int y = hh; y; --y, dst += dx, src += sx2) {
            const uchar *p1 = src;
            const uchar *p2 = src + sx;
            uchar *q = dst;
            for (int x = ww; x; --x, ++q, p1 += 2, p2 += 2)
                *q = ((int(p1[0]) + int(p1[1]) + int(p2[0]) + int(p2[1])) + 2) >> 2;
        }

        return dest;

    } else if (source.format() == QImage::Format_ARGB8565_Premultiplied) {
        QImage dest(source.width() / 2, source.height() / 2, srcImage.format());
        dest.setDevicePixelRatio(source.devicePixelRatioF());

        const uchar *src = reinterpret_cast<const uchar*>(const_cast<const QImage &>(srcImage).bits());
        int sx = srcImage.bytesPerLine();
        int sx2 = sx << 1;

        uchar *dst = reinterpret_cast<uchar*>(dest.bits());
        int dx = dest.bytesPerLine();
        int ww = dest.width();
        int hh = dest.height();

        for (int y = hh; y; --y, dst += dx, src += sx2) {
            const uchar *p1 = src;
            const uchar *p2 = src + sx;
            uchar *q = dst;
            for (int x = ww; x; --x, q += 3, p1 += 6, p2 += 6) {
                q[0] = AVG(AVG(p1[0], p1[3]), AVG(p2[0], p2[3]));
                const quint16 p16_1 = (p1[2] << 8) | p1[1];
                const quint16 p16_2 = (p1[5] << 8) | p1[4];
                const quint16 p16_3 = (p2[2] << 8) | p2[1];
                const quint16 p16_4 = (p2[5] << 8) | p2[4];
                const quint16 result = AVG16(AVG16(p16_1, p16_2), AVG16(p16_3, p16_4));
                q[1] = result & 0xff;
                q[2] = result >> 8;
            }
        }

        return dest;
    } else if (source.format() != QImage::Format_ARGB32_Premultiplied && source.format() != QImage::Format_RGB32) {
        srcImage = source.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    }

    QImage dest(source.width() / 2, source.height() / 2, srcImage.format());
    dest.setDevicePixelRatio(source.devicePixelRatioF());

    const quint32 *src = reinterpret_cast<const quint32*>(const_cast<const QImage &>(srcImage).bits());
    int sx = srcImage.bytesPerLine() >> 2;
    int sx2 = sx << 1;

    quint32 *dst = reinterpret_cast<quint32*>(dest.bits());
    int dx = dest.bytesPerLine() >> 2;
    int ww = dest.width();
    int hh = dest.height();

    for (int y = hh; y; --y, dst += dx, src += sx2) {
        const quint32 *p1 = src;
        const quint32 *p2 = src + sx;
        quint32 *q = dst;
        for (int x = ww; x; --x, q++, p1 += 2, p2 += 2)
            *q = AVG(AVG(p1[0], p1[1]), AVG(p2[0], p2[1]));
    }

    return dest;
}

void dtk_blurImage(QPainter *p, QImage &blurImage, qreal radius, bool quality, bool alphaOnly, int transposed = 0)
{
    if (blurImage.format() != QImage::Format_ARGB32_Premultiplied
        && blurImage.format() != QImage::Format_RGB32)
    {
        blurImage = blurImage.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    }

    qreal scale = 1;
    if (radius >= 4 && blurImage.width() >= 2 && blurImage.height() >= 2) {
        blurImage = dtk_halfScaled(blurImage);
        scale = 2;
        radius *= qreal(0.5);
    }

    if (alphaOnly)
        expblur<12, 10, true>(blurImage, radius, quality, transposed);
    else
        expblur<12, 10, false>(blurImage, radius, quality, transposed);

    if (p) {
        p->scale(scale, scale);
        p->setRenderHint(QPainter::SmoothPixmapTransform);
        p->drawImage(QRect(QPoint(0, 0), blurImage.size() / blurImage.devicePixelRatioF()), blurImage);
    }
}

void dtk_blurImage(QImage &blurImage, qreal radius, bool quality, int transposed = 0)
{
    if (blurImage.format() == QImage::Format_Indexed8 || blurImage.format() == QImage::Format_Grayscale8)
        expblur<12, 10, true>(blurImage, radius, quality, transposed);
    else
        expblur<12, 10, false>(blurImage, radius, quality, transposed);
}

DTKWIDGETS_EXPORT bool dtk_scaleForTransform(const QTransform &transform, qreal *scale)
{
    const QTransform::TransformationType type = transform.type();

    if (type <= QTransform::TxTranslate) {
        if (scale)
            *scale = 1;
        return true;
    } else if (type == QTransform::TxScale) {
        const qreal xScale = qAbs(transform.m11());
        const qreal yScale = qAbs(transform.m22());
        if (scale)
            *scale = qMax(xScale, yScale);
        return qFuzzyCompare(xScale, yScale);
    }

    const qreal xScale1 = transform.m11() * transform.m11()
                         + transform.m21() * transform.m21();
    const qreal yScale1 = transform.m12() * transform.m12()
                         + transform.m22() * transform.m22();
    const qreal xScale2 = transform.m11() * transform.m11()
                         + transform.m12() * transform.m12();
    const qreal yScale2 = transform.m21() * transform.m21()
                         + transform.m22() * transform.m22();

    if (qAbs(xScale1 - yScale1) > qAbs(xScale2 - yScale2)) {
        if (scale)
            *scale = qSqrt(qMax(xScale1, yScale1));

        return type == QTransform::TxRotate && qFuzzyCompare(xScale1, yScale1);
    } else {
        if (scale)
            *scale = qSqrt(qMax(xScale2, yScale2));

        return type == QTransform::TxRotate && qFuzzyCompare(xScale2, yScale2);
    }
}

void dtkPixmapBlurFilter::draw(QPainter *painter, const QPointF &p, const QPixmap &src, const QRectF &rect) const
{
    if (!painter->isActive())
        return;

    if (src.isNull())
        return;

    QRectF srcRect = rect;
    if (srcRect.isNull())
        srcRect = src.rect();

    if (d->radius <= 1) {
        painter->drawPixmap(srcRect.translated(p), src, srcRect);
        return;
    }

    qreal scaledRadius = radiusScale * d->radius;
    qreal scale;

    if (dtk_scaleForTransform(painter->transform(), &scale))
        scaledRadius /= scale;

    QImage srcImage;

    if (srcRect == src.rect()) {
        srcImage = src.toImage();
    } else {
        QRect rect = srcRect.toAlignedRect().intersected(src.rect());
        srcImage = src.copy(rect).toImage();
    }

    QTransform transform = painter->worldTransform();
    painter->translate(p);
    dtk_blurImage(painter, srcImage, scaledRadius, (d->hints & dtkGraphicsBlurEffect::QualityHint), false);
    painter->setWorldTransform(transform);
}

static void grayscale(const QImage &image, QImage &dest, const QRect& rect = QRect())
{
    QRect destRect = rect;
    QRect srcRect = rect;

    if (rect.isNull()) {
        srcRect = dest.rect();
        destRect = dest.rect();
    }

    if (&image != &dest) {
        destRect.moveTo(QPoint(0, 0));
    }

    const unsigned int *data = (const unsigned int *)image.bits();

    unsigned int *outData = (unsigned int *)dest.bits();

    if (dest.size() == image.size() && image.rect() == srcRect) {
        // a bit faster loop for grayscaling everything
        int pixels = dest.width() * dest.height();
        for (int i = 0; i < pixels; ++i) {
            int val = qGray(data[i]);
            outData[i] = qRgba(val, val, val, qAlpha(data[i]));
        }
    } else {
        int yd = destRect.top();
        for (int y = srcRect.top(); y <= srcRect.bottom() && y < image.height(); y++) {
            data = (const unsigned int*)image.scanLine(y);
            outData = (unsigned int*)dest.scanLine(yd++);
            int xd = destRect.left();
            for (int x = srcRect.left(); x <= srcRect.right() && x < image.width(); x++) {
                int val = qGray(data[x]);
                outData[xd++] = qRgba(val, val, val, qAlpha(data[x]));
            }
        }
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapColorizeFilterPrivate : public dtkPixmapFilterPrivate
{
public:
    QColor color;
    qreal strength;
    quint32 opaque : 1;
    quint32 alphaBlend : 1;
    quint32 padding : 30;

public:
    dtkPixmapColorizeFilter *q = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkPixmapColorizeFilter::dtkPixmapColorizeFilter(QObject *parent) : dtkPixmapFilter(*new dtkPixmapColorizeFilterPrivate, ColorizeFilter, parent)
{
    d->color = QColor(0, 0, 192);
    d->strength = qreal(1);
    d->opaque = true;
    d->alphaBlend = false;
}

dtkPixmapColorizeFilter::~dtkPixmapColorizeFilter(void)
{

}

QColor dtkPixmapColorizeFilter::color(void) const
{
    return d->color;
}

void dtkPixmapColorizeFilter::setColor(const QColor &color)
{
    d->color = color;
}

qreal dtkPixmapColorizeFilter::strength() const
{
    return d->strength;
}

void dtkPixmapColorizeFilter::setStrength(qreal strength)
{
    d->strength = qBound(qreal(0), strength, qreal(1));
    d->opaque = !qFuzzyIsNull(d->strength);
    d->alphaBlend = !qFuzzyIsNull(d->strength - 1);
}

void dtkPixmapColorizeFilter::draw(QPainter *painter, const QPointF &dest, const QPixmap &src, const QRectF &srcRect) const
{
    if (src.isNull())
        return;

    if (!d->opaque) {
        painter->drawPixmap(dest, src, srcRect);
        return;
    }

    QImage srcImage;
    QImage destImage;

    if (srcRect.isNull()) {
        srcImage = src.toImage();
        const auto format = srcImage.hasAlphaChannel() ? QImage::Format_ARGB32_Premultiplied : QImage::Format_RGB32;
        srcImage = std::move(srcImage).convertToFormat(format);
        destImage = QImage(srcImage.size(), srcImage.format());
    } else {
        QRect rect = srcRect.toAlignedRect().intersected(src.rect());

        srcImage = src.copy(rect).toImage();
        const auto format = srcImage.hasAlphaChannel() ? QImage::Format_ARGB32_Premultiplied : QImage::Format_RGB32;
        srcImage = std::move(srcImage).convertToFormat(format);
        destImage = QImage(rect.size(), srcImage.format());
    }
    destImage.setDevicePixelRatio(src.devicePixelRatioF());

    QPainter destPainter(&destImage);
    grayscale(srcImage, destImage, srcImage.rect());
    destPainter.setCompositionMode(QPainter::CompositionMode_Screen);
    destPainter.fillRect(srcImage.rect(), d->color);
    destPainter.end();

    if (d->alphaBlend) {
        QImage buffer = srcImage;
        QPainter bufPainter(&buffer);
        bufPainter.setOpacity(d->strength);
        bufPainter.drawImage(0, 0, destImage);
        bufPainter.end();
        destImage = std::move(buffer);
    }

    if (srcImage.hasAlphaChannel())
        destImage.setAlphaChannel(srcImage.alphaChannel());

    painter->drawImage(dest, destImage);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkPixmapDropShadowFilterPrivate : public dtkPixmapFilterPrivate
{
public:
    dtkPixmapDropShadowFilterPrivate() : offset(8, 8), color(63, 63, 63, 180), radius(1) {}

    QPointF offset;
    QColor color;
    qreal radius;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////
//
dtkPixmapDropShadowFilter::dtkPixmapDropShadowFilter(QObject *parent) : dtkPixmapFilter(*new dtkPixmapDropShadowFilterPrivate, DropShadowFilter, parent)
{

}

dtkPixmapDropShadowFilter::~dtkPixmapDropShadowFilter(void)
{

}

qreal dtkPixmapDropShadowFilter::blurRadius(void) const
{
    return d->radius;
}

void dtkPixmapDropShadowFilter::setBlurRadius(qreal radius)
{
    d->radius = radius;
}

QColor dtkPixmapDropShadowFilter::color(void) const
{
    return d->color;
}

void dtkPixmapDropShadowFilter::setColor(const QColor &color)
{
    d->color = color;
}

QPointF dtkPixmapDropShadowFilter::offset(void) const
{
    return d->offset;
}

void dtkPixmapDropShadowFilter::setOffset(const QPointF &offset)
{
    d->offset = offset;
}

QRectF dtkPixmapDropShadowFilter::boundingRectFor(const QRectF &rect) const
{
    return rect.united(rect.translated(d->offset).adjusted(-d->radius, -d->radius, d->radius, d->radius));
}

void dtkPixmapDropShadowFilter::draw(QPainter *p, const QPointF &pos, const QPixmap &px, const QRectF &src) const
{
    if (px.isNull())
        return;

    QImage tmp(px.size(), QImage::Format_ARGB32_Premultiplied);
    tmp.setDevicePixelRatio(px.devicePixelRatioF());
    tmp.fill(0);

    QPainter tmpPainter(&tmp);
    tmpPainter.setCompositionMode(QPainter::CompositionMode_Source);
    tmpPainter.drawPixmap(d->offset, px);
    tmpPainter.end();

    QImage blurred(tmp.size(), QImage::Format_ARGB32_Premultiplied);
    blurred.setDevicePixelRatio(px.devicePixelRatioF());
    blurred.fill(0);
    QPainter blurPainter(&blurred);
    dtk_blurImage(&blurPainter, tmp, d->radius, false, true);
    blurPainter.end();

    tmp = std::move(blurred);

    tmpPainter.begin(&tmp);
    tmpPainter.setCompositionMode(QPainter::CompositionMode_SourceIn);
    tmpPainter.fillRect(tmp.rect(), d->color);
    tmpPainter.end();

    p->drawImage(pos, tmp);
    p->drawPixmap(pos, px, src);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsColorizeEffectPrivate;

class DTKWIDGETS_EXPORT dtkGraphicsColorizeEffect: public dtkGraphicsEffect
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(qreal strength READ strength WRITE setStrength NOTIFY strengthChanged)
public:
    dtkGraphicsColorizeEffect(QObject *parent = nullptr);
    ~dtkGraphicsColorizeEffect();

    QColor color() const;
    qreal strength() const;

public Q_SLOTS:
    void setColor(const QColor &c);
    void setStrength(qreal strength);

Q_SIGNALS:
    void colorChanged(const QColor &color);
    void strengthChanged(qreal strength);

protected:
    void draw(QPainter *painter) override;

private:
    Q_DISABLE_COPY(dtkGraphicsColorizeEffect);

private:
    dtkGraphicsColorizeEffectPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsDropShadowEffectPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkGraphicsDropShadowEffect: public dtkGraphicsEffect
{
    Q_OBJECT
    Q_PROPERTY(QPointF offset READ offset WRITE setOffset NOTIFY offsetChanged)
    Q_PROPERTY(qreal xOffset READ xOffset WRITE setXOffset NOTIFY offsetChanged)
    Q_PROPERTY(qreal yOffset READ yOffset WRITE setYOffset NOTIFY offsetChanged)
    Q_PROPERTY(qreal blurRadius READ blurRadius WRITE setBlurRadius NOTIFY blurRadiusChanged)
    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
     dtkGraphicsDropShadowEffect(QObject *parent = nullptr);
    ~dtkGraphicsDropShadowEffect();

    QRectF boundingRectFor(const QRectF &rect) const override;
    QPointF offset() const;

    inline qreal xOffset() const
    {
        return offset().x();
    }

    inline qreal yOffset() const
    {
        return offset().y();
    }

    qreal blurRadius() const;
    QColor color() const;

public Q_SLOTS:
    void setOffset(const QPointF &ofs);

    inline void setOffset(qreal dx, qreal dy)
    { setOffset(QPointF(dx, dy)); }

    inline void setOffset(qreal d)
    { setOffset(QPointF(d, d)); }

    inline void setXOffset(qreal dx)
    { setOffset(QPointF(dx, yOffset())); }

    inline void setYOffset(qreal dy)
    { setOffset(QPointF(xOffset(), dy)); }

    void setBlurRadius(qreal blurRadius);
    void setColor(const QColor &color);

Q_SIGNALS:
    void offsetChanged(const QPointF &offset);
    void blurRadiusChanged(qreal blurRadius);
    void colorChanged(const QColor &color);

protected:
    void draw(QPainter *painter) override;

private:
    Q_DISABLE_COPY(dtkGraphicsDropShadowEffect);

private:
    dtkGraphicsDropShadowEffectPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsOpacityEffectPrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkGraphicsOpacityEffect: public dtkGraphicsEffect
{
    Q_OBJECT
    Q_PROPERTY(qreal opacity READ opacity WRITE setOpacity NOTIFY opacityChanged)
    Q_PROPERTY(QBrush opacityMask READ opacityMask WRITE setOpacityMask NOTIFY opacityMaskChanged)

public:
     dtkGraphicsOpacityEffect(QObject *parent = nullptr);
    ~dtkGraphicsOpacityEffect();

    qreal opacity() const;
    QBrush opacityMask() const;

public Q_SLOTS:
    void setOpacity(qreal opacity);
    void setOpacityMask(const QBrush &mask);

Q_SIGNALS:
    void opacityChanged(qreal opacity);
    void opacityMaskChanged(const QBrush &mask);

protected:
    void draw(QPainter *painter) override;

private:
    Q_DISABLE_COPY(dtkGraphicsOpacityEffect);

private:
    dtkGraphicsOpacityEffectPrivate *d;
};

// ///////////////////////////////////////////////////////////////////
// Wrappers
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsEffectSource;

class dtkGraphicsEffectSourcePrivate
{
public:
    dtkGraphicsEffectSourcePrivate(void) {}

    enum InvalidateReason
    {
        TransformChanged,
        EffectRectChanged,
        SourceChanged
    };

    virtual ~dtkGraphicsEffectSourcePrivate();
    virtual void detach() = 0;
    virtual QRectF boundingRect(Qt::CoordinateSystem system) const = 0;
    virtual QRect deviceRect() const = 0;
    virtual const QWidget *widget() const = 0;
    virtual const QStyleOption *styleOption() const = 0;
    virtual void draw(QPainter *p) = 0;
    virtual void update() = 0;
    virtual bool isPixmap() const = 0;
    virtual QPixmap pixmap(Qt::CoordinateSystem system, QPoint *offset = 0, dtkGraphicsEffect::PixmapPadMode mode = dtkGraphicsEffect::PadToTransparentBorder) const = 0;
    virtual void effectBoundingRectChanged() = 0;

    void setCachedOffset(const QPoint &offset);
    void invalidateCache(InvalidateReason reason = SourceChanged) const;
    Qt::CoordinateSystem currentCachedSystem() const { return m_cachedSystem; }
    dtkGraphicsEffect::PixmapPadMode currentCachedMode() const { return m_cachedMode; }

private:
    mutable Qt::CoordinateSystem m_cachedSystem;
    mutable dtkGraphicsEffect::PixmapPadMode m_cachedMode;
    mutable QPoint m_cachedOffset;
    mutable QPixmapCache::Key m_cacheKey;

private:
    dtkGraphicsEffectSource *q = nullptr;

private:
    friend class dtkGraphicsEffectSource;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkGraphicsEffectSource : public QObject
{
    Q_OBJECT

public:
    ~dtkGraphicsEffectSource(void);

public:
    const QWidget *widget(void) const;
    const QStyleOption *styleOption(void) const;

    bool isPixmap(void) const;
    void draw(QPainter *painter);
    void update(void);

    QRectF boundingRect(Qt::CoordinateSystem coordinateSystem = Qt::LogicalCoordinates) const;
    QRect deviceRect(void) const;
    QPixmap pixmap(Qt::CoordinateSystem system = Qt::LogicalCoordinates, QPoint *offset = 0, dtkGraphicsEffect::PixmapPadMode mode = dtkGraphicsEffect::PadToEffectiveBoundingRect) const;

protected:
    dtkGraphicsEffectSource(dtkGraphicsEffectSourcePrivate &dd, QObject *parent = 0);

private:
    Q_DISABLE_COPY(dtkGraphicsEffectSource);

private:
    friend class dtkGraphicsEffect;
    friend class dtkGraphicsEffectPrivate;

private:
    dtkGraphicsEffectSourcePrivate *d;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkGraphicsEffectPrivate
{
public:
     dtkGraphicsEffectPrivate(void) : source(0), isEnabled(1) {}
    ~dtkGraphicsEffectPrivate(void);

public:
    dtkGraphicsEffectSource *source;

public:
    QRectF boundingRect;

public:
    quint32 isEnabled : 1;
    quint32 padding : 31; // feel free to use

public:
    dtkGraphicsEffect *q;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsColorizeEffectPrivate : public dtkGraphicsEffectPrivate
{
public:
    dtkGraphicsColorizeEffectPrivate(void) : opaque(true)
    {
        filter = new dtkPixmapColorizeFilter;
    }

    ~dtkGraphicsColorizeEffectPrivate(void)
    {
        delete filter;
    }

    dtkPixmapColorizeFilter *filter;

    quint32 opaque : 1;
    quint32 padding : 31;

public:
    dtkGraphicsColorizeEffect *q = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsBlurEffectPrivate : public dtkGraphicsEffectPrivate
{
public:
     dtkGraphicsBlurEffectPrivate(void) : filter(new dtkPixmapBlurFilter) {}
    ~dtkGraphicsBlurEffectPrivate(void) { delete filter; }

public:
    dtkPixmapBlurFilter *filter;

public:
    dtkGraphicsBlurEffect *q = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsDropShadowEffectPrivate : public dtkGraphicsEffectPrivate
{
public:
     dtkGraphicsDropShadowEffectPrivate(void) : filter(new dtkPixmapDropShadowFilter) {}
    ~dtkGraphicsDropShadowEffectPrivate(void) { delete filter; }

public:
    dtkPixmapDropShadowFilter *filter;

public:
    dtkGraphicsDropShadowEffect *q = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkGraphicsOpacityEffectPrivate : public dtkGraphicsEffectPrivate
{
public:
     dtkGraphicsOpacityEffectPrivate(void) : opacity(qreal(0.7)), isFullyTransparent(0), isFullyOpaque(0), hasOpacityMask(0) {}
    ~dtkGraphicsOpacityEffectPrivate(void) {}

public:
    qreal opacity;
    QBrush opacityMask;
    uint isFullyTransparent : 1;
    uint isFullyOpaque : 1;
    uint hasOpacityMask : 1;

public:
    dtkGraphicsOpacityEffect *q = nullptr;
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsEffectPrivate::~dtkGraphicsEffectPrivate(void)
{

}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsEffectSource::dtkGraphicsEffectSource(dtkGraphicsEffectSourcePrivate &dd, QObject *parent) : QObject(parent)
{
    d = &dd;
}

dtkGraphicsEffectSource::~dtkGraphicsEffectSource(void)
{

}

QRectF dtkGraphicsEffectSource::boundingRect(Qt::CoordinateSystem system) const
{
    return d->boundingRect(system);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

QRectF dtkGraphicsEffect::sourceBoundingRect(Qt::CoordinateSystem system) const
{
    if (d->source)
        return d->source->boundingRect(system);

    return QRectF();
}

void dtkGraphicsEffect::setGraphicsEffectSource(dtkGraphicsEffectSource *newSource)
{
    dtkGraphicsEffect::ChangeFlags flags;

    if (d->source) {
        flags |= dtkGraphicsEffect::SourceDetached;
        d->source->d->invalidateCache();
        d->source->d->detach();
        delete d->source;
    }

    d->source = newSource;

    if (newSource)
        flags |= dtkGraphicsEffect::SourceAttached;

    this->sourceChanged(flags);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

const QWidget *dtkGraphicsEffectSource::widget(void) const
{
    return d->widget();
}

const QStyleOption *dtkGraphicsEffectSource::styleOption(void) const
{
    return d->styleOption();
}

void dtkGraphicsEffectSource::draw(QPainter *painter)
{
    QPixmap pm;

    if (QPixmapCache::find(d->m_cacheKey, &pm)) {

        QTransform restoreTransform;

        if (d->m_cachedSystem == Qt::DeviceCoordinates) {
            restoreTransform = painter->worldTransform();
            painter->setWorldTransform(QTransform());
        }

        painter->drawPixmap(d->m_cachedOffset, pm);

        if (d->m_cachedSystem == Qt::DeviceCoordinates)
            painter->setWorldTransform(restoreTransform);
    } else {
        d->draw(painter);
    }
}

void dtkGraphicsEffect::drawSource(QPainter *painter)
{
    if (d->source)
        d->source->draw(painter);
}

void dtkGraphicsEffectSource::update(void)
{
    d->update();
}

bool dtkGraphicsEffectSource::isPixmap(void) const
{
    return d->isPixmap();
}

bool dtkGraphicsEffect::sourceIsPixmap(void) const
{
    return source() ? source()->isPixmap() : false;
}

QPixmap dtkGraphicsEffectSource::pixmap(Qt::CoordinateSystem system, QPoint *offset, dtkGraphicsEffect::PixmapPadMode mode) const
{
    QPixmap pm;

    if (pm.isNull()) {
        pm = d->pixmap(system, &d->m_cachedOffset, mode);
        d->m_cachedSystem = system;
        d->m_cachedMode = mode;

        d->invalidateCache();
        d->m_cacheKey = QPixmapCache::insert(pm);
    }

    if (offset)
        *offset = d->m_cachedOffset;

    return pm;
}

QPixmap dtkGraphicsEffect::sourcePixmap(Qt::CoordinateSystem system, QPoint *offset, dtkGraphicsEffect::PixmapPadMode mode) const
{
    if (d->source)
        return d->source->pixmap(system, offset, mode);

    return QPixmap();
}

dtkGraphicsEffectSourcePrivate::~dtkGraphicsEffectSourcePrivate(void)
{
    invalidateCache();
}

void dtkGraphicsEffectSourcePrivate::setCachedOffset(const QPoint &offset)
{
    m_cachedOffset = offset;
}

void dtkGraphicsEffectSourcePrivate::invalidateCache(InvalidateReason reason) const
{
    if (m_cachedMode != dtkGraphicsEffect::PadToEffectiveBoundingRect && (reason == EffectRectChanged || (reason == TransformChanged && m_cachedSystem == Qt::LogicalCoordinates))) {
        return;
    }

    QPixmapCache::remove(m_cacheKey);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsEffect::dtkGraphicsEffect(QObject *parent) : QObject(parent)
{
    d = new dtkGraphicsEffectPrivate;
}

dtkGraphicsEffect::dtkGraphicsEffect(dtkGraphicsEffectPrivate &dd, QObject *parent) : QObject(parent)
{
    d = &dd;
}

dtkGraphicsEffect::~dtkGraphicsEffect(void)
{
    delete d;
}

QRectF dtkGraphicsEffect::boundingRect(void) const
{
    if (d->source)
        return boundingRectFor(d->source->boundingRect());

    return QRectF();
}

QRectF dtkGraphicsEffect::boundingRectFor(const QRectF &rect) const
{
    return rect;
}

bool dtkGraphicsEffect::isEnabled(void) const
{
    return d->isEnabled;
}

void dtkGraphicsEffect::setEnabled(bool enable)
{
    if (d->isEnabled == enable)
        return;

    d->isEnabled = enable;

    if (d->source) {
        d->source->d->effectBoundingRectChanged();
        d->source->d->invalidateCache();
    }

    emit enabledChanged(enable);
}

void dtkGraphicsEffect::update(void)
{
    if (d->source)
        d->source->update();
}

dtkGraphicsEffectSource *dtkGraphicsEffect::source(void) const
{
    return d->source;
}

void dtkGraphicsEffect::updateBoundingRect(void)
{
    if (d->source) {
        d->source->d->effectBoundingRectChanged();
        d->source->d->invalidateCache(dtkGraphicsEffectSourcePrivate::EffectRectChanged);
    }
}

void dtkGraphicsEffect::sourceChanged(ChangeFlags flags)
{
    Q_UNUSED(flags);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsColorizeEffect::dtkGraphicsColorizeEffect(QObject *parent) : dtkGraphicsEffect(*new dtkGraphicsColorizeEffectPrivate, parent)
{

}

dtkGraphicsColorizeEffect::~dtkGraphicsColorizeEffect(void)
{

}

QColor dtkGraphicsColorizeEffect::color(void) const
{
    return d->filter->color();
}

void dtkGraphicsColorizeEffect::setColor(const QColor &color)
{
    if (d->filter->color() == color)
        return;

    d->filter->setColor(color);

    update();

    emit colorChanged(color);
}

qreal dtkGraphicsColorizeEffect::strength(void) const
{
    return d->filter->strength();
}

void dtkGraphicsColorizeEffect::setStrength(qreal strength)
{
    if (qFuzzyCompare(d->filter->strength(), strength))
        return;

    d->filter->setStrength(strength);

    d->opaque = !qFuzzyIsNull(strength);

    update();

    emit strengthChanged(strength);
}

void dtkGraphicsColorizeEffect::draw(QPainter *painter)
{
    if (!d->opaque) {
        drawSource(painter);
        return;
    }

    QPoint offset;

    if (sourceIsPixmap()) {
        const QPixmap pixmap = sourcePixmap(Qt::LogicalCoordinates, &offset, NoPad);

        if (!pixmap.isNull())
            d->filter->draw(painter, offset, pixmap);

        return;
    }

    const QPixmap pixmap = sourcePixmap(Qt::DeviceCoordinates, &offset);

    if (pixmap.isNull())
        return;

    QTransform restoreTransform = painter->worldTransform();
    painter->setWorldTransform(QTransform());
    d->filter->draw(painter, offset, pixmap);
    painter->setWorldTransform(restoreTransform);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsBlurEffect::dtkGraphicsBlurEffect(QObject *parent) : dtkGraphicsEffect(parent)
{
    qDebug() << Q_FUNC_INFO << 0;
    d = new dtkGraphicsBlurEffectPrivate;
    qDebug() << Q_FUNC_INFO << 1 << d;
    d->filter->setBlurHints(dtkGraphicsBlurEffect::PerformanceHint);
    qDebug() << Q_FUNC_INFO << 2;
}

dtkGraphicsBlurEffect::~dtkGraphicsBlurEffect(void)
{
    delete d;
}

qreal dtkGraphicsBlurEffect::blurRadius(void) const
{
    return d->filter->radius();
}

void dtkGraphicsBlurEffect::setBlurRadius(qreal radius)
{
    if (qFuzzyCompare(d->filter->radius(), radius))
        return;

    d->filter->setRadius(radius);

    updateBoundingRect();

    emit blurRadiusChanged(radius);
}

dtkGraphicsBlurEffect::BlurHints dtkGraphicsBlurEffect::blurHints(void) const
{
    return d->filter->blurHints();
}

void dtkGraphicsBlurEffect::setBlurHints(dtkGraphicsBlurEffect::BlurHints hints)
{
    if (d->filter->blurHints() == hints)
        return;

    d->filter->setBlurHints(hints);

    emit blurHintsChanged(hints);
}

QRectF dtkGraphicsBlurEffect::boundingRectFor(const QRectF &rect) const
{
    return d->filter->boundingRectFor(rect);
}

void dtkGraphicsBlurEffect::draw(QPainter *painter)
{
    if (d->filter->radius() < 1) {
        drawSource(painter);
        return;
    }

    PixmapPadMode mode = PadToEffectiveBoundingRect;

    QPoint offset;
    QPixmap pixmap = sourcePixmap(Qt::LogicalCoordinates, &offset, mode);

    if (pixmap.isNull())
        return;

    d->filter->draw(painter, offset, pixmap);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsDropShadowEffect::dtkGraphicsDropShadowEffect(QObject *parent) : dtkGraphicsEffect(*new dtkGraphicsDropShadowEffectPrivate, parent)
{

}

dtkGraphicsDropShadowEffect::~dtkGraphicsDropShadowEffect(void)
{

}

QPointF dtkGraphicsDropShadowEffect::offset(void) const
{
    return d->filter->offset();
}

void dtkGraphicsDropShadowEffect::setOffset(const QPointF &offset)
{
    if (d->filter->offset() == offset)
        return;

    d->filter->setOffset(offset);

    updateBoundingRect();

    emit offsetChanged(offset);
}

qreal dtkGraphicsDropShadowEffect::blurRadius(void) const
{
    return d->filter->blurRadius();
}

void dtkGraphicsDropShadowEffect::setBlurRadius(qreal blurRadius)
{
    if (qFuzzyCompare(d->filter->blurRadius(), blurRadius))
        return;

    d->filter->setBlurRadius(blurRadius);

    updateBoundingRect();

    emit blurRadiusChanged(blurRadius);
}

QColor dtkGraphicsDropShadowEffect::color(void) const
{
    return d->filter->color();
}

void dtkGraphicsDropShadowEffect::setColor(const QColor &color)
{
    if (d->filter->color() == color)
        return;

    d->filter->setColor(color);

    update();

    emit colorChanged(color);
}

QRectF dtkGraphicsDropShadowEffect::boundingRectFor(const QRectF &rect) const
{
    return d->filter->boundingRectFor(rect);
}

void dtkGraphicsDropShadowEffect::draw(QPainter *painter)
{
    if (d->filter->blurRadius() <= 0 && d->filter->offset().isNull()) {
        drawSource(painter);
        return;
    }

    PixmapPadMode mode = PadToEffectiveBoundingRect;

    QPoint offset;

    const QPixmap pixmap = sourcePixmap(Qt::DeviceCoordinates, &offset, mode);

    if (pixmap.isNull())
        return;

    QTransform restoreTransform = painter->worldTransform();
    painter->setWorldTransform(QTransform());
    d->filter->draw(painter, offset, pixmap);
    painter->setWorldTransform(restoreTransform);
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkGraphicsOpacityEffect::dtkGraphicsOpacityEffect(QObject *parent) : dtkGraphicsEffect(*new dtkGraphicsOpacityEffectPrivate, parent)
{

}

dtkGraphicsOpacityEffect::~dtkGraphicsOpacityEffect(void)
{

}

qreal dtkGraphicsOpacityEffect::opacity(void) const
{
    return d->opacity;
}

void dtkGraphicsOpacityEffect::setOpacity(qreal opacity)
{
    opacity = qBound(qreal(0.0), opacity, qreal(1.0));

    if (qFuzzyCompare(d->opacity, opacity))
        return;

    d->opacity = opacity;

    if ((d->isFullyTransparent = qFuzzyIsNull(d->opacity)))
        d->isFullyOpaque = 0;
    else
        d->isFullyOpaque = qFuzzyIsNull(d->opacity - 1);

    update();

    emit opacityChanged(opacity);
}

QBrush dtkGraphicsOpacityEffect::opacityMask(void) const
{
    return d->opacityMask;
}

void dtkGraphicsOpacityEffect::setOpacityMask(const QBrush &mask)
{
    if (d->opacityMask == mask)
        return;

    d->opacityMask = mask;

    d->hasOpacityMask = (mask.style() != Qt::NoBrush);

    update();

    emit opacityMaskChanged(mask);
}

void dtkGraphicsOpacityEffect::draw(QPainter *painter)
{
    if (d->isFullyTransparent)
        return;

    if (d->isFullyOpaque && !d->hasOpacityMask) {
        drawSource(painter);
        return;
    }

    QPoint offset;

    Qt::CoordinateSystem system = sourceIsPixmap() ? Qt::LogicalCoordinates : Qt::DeviceCoordinates;

    QPixmap pixmap = sourcePixmap(system, &offset, dtkGraphicsEffect::NoPad);

    if (pixmap.isNull())
        return;

    painter->save();
    painter->setOpacity(d->opacity);

    if (d->hasOpacityMask) {
        QPainter pixmapPainter(&pixmap);
        pixmapPainter.setRenderHints(painter->renderHints());
        pixmapPainter.setCompositionMode(QPainter::CompositionMode_DestinationIn);
        if (system == Qt::DeviceCoordinates) {
            QTransform worldTransform = painter->worldTransform();
            worldTransform *= QTransform::fromTranslate(-offset.x(), -offset.y());
            pixmapPainter.setWorldTransform(worldTransform);
            pixmapPainter.fillRect(sourceBoundingRect(), d->opacityMask);
        } else {
            pixmapPainter.translate(-offset);
            pixmapPainter.fillRect(pixmap.rect(), d->opacityMask);
        }
    }

    if (system == Qt::DeviceCoordinates)
        painter->setWorldTransform(QTransform());

    painter->drawPixmap(offset, pixmap);
    painter->restore();
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsPassThrough : public QFrame
{
    Q_OBJECT

public:
    dtkWidgetsPassThrough(QWidget *parent = nullptr);

public:
    QSize sizeHint(void) const override
    {
        return QSize(200, 200);
    }

protected slots:
    void grab(void);

protected:
    void paintEvent(QPaintEvent *) override;

protected:
    void mousePressEvent(QMouseEvent *event) override
    {
        this->ignre = true;

        this->o_pos = event->globalPos();
    }

    void mouseMoveEvent(QMouseEvent *event) override
    {
        this->c_pos = event->globalPos();

        this->d_pos = this->c_pos - this->o_pos;

        this->topLevelWidget()->move(this->topLevelWidget()->pos() + this->d_pos);

        this->o_pos = this->c_pos;
    }

    void mouseReleaseEvent(QMouseEvent *) override
    {
        this->ignre = false;

        this->grab();
    }


private:
    QPixmap buffer;

public:
    QPoint o_pos;
    QPoint c_pos;
    QPoint d_pos;

public:
    bool ignre = false;
    bool dirty = false;
};

dtkWidgetsPassThrough::dtkWidgetsPassThrough(QWidget *parent) : QFrame(parent)
{
    this->setAttribute(Qt::WA_NoSystemBackground);
    this->setAttribute(Qt::WA_TranslucentBackground);

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    this->setWindowFlags(Qt::FramelessWindowHint);
}

void dtkWidgetsPassThrough::grab(void)
{
    this->dirty = true;

    this->repaint();

    qApp->processEvents();

    QScreen *screen = qApp->primaryScreen();

    if (const QWindow *window = windowHandle())
        screen = window->screen();

    if (!screen)
        return;

    QPoint origin = this->mapToGlobal(QPoint(0, 0));

    qreal s = screen->devicePixelRatio();
    qreal d = 0; // -50;

    this->buffer = screen->grabWindow(0).copy(d+s*origin.x(), d+s*origin.y(), s*this->width(), s*this->height());

    this->dirty = false;

    this->repaint();
}

void dtkWidgetsPassThrough::paintEvent(QPaintEvent *event)
{
    if (this->ignre)
        return;

    QPainter painter(this);

    if(!this->dirty) {

// ///////////////////////////////////////////////////////////////////
// Aiming at goal
// ///////////////////////////////////////////////////////////////////

        dtkGraphicsBlurEffect effect(this);
        effect.draw(&painter);

// ///////////////////////////////////////////////////////////////////
//      painter.drawPixmap(event->rect(), buffer);
// ///////////////////////////////////////////////////////////////////
        painter.fillRect(event->rect(), QColor(64, 0, 64, 128));
        painter.setPen(QPen(Qt::yellow, 50));
        painter.drawLine(event->rect().topLeft(), event->rect().topRight());
        painter.drawLine(event->rect().topRight(), event->rect().bottomRight());
        painter.drawLine(event->rect().bottomRight(), event->rect().bottomLeft());
        painter.drawLine(event->rect().bottomLeft(), event->rect().topLeft());
    } else {
        painter.fillRect(event->rect(), QColor(0, 0, 0, 0));
    }
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dummy : public dtkWidgetsMainWindow
{
public:
     dummy(QWidget *parent = nullptr);
    ~dummy(void);

protected:
    void paintEvent(QPaintEvent *) override;
};

dummy::dummy(QWidget *parent) : dtkWidgetsMainWindow(parent)
{
    QFrame *central = new QFrame(this);

    // this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    // this->setAttribute(Qt::WA_NoSystemBackground, true);
    // this->setAttribute(Qt::WA_TranslucentBackground, true);

    // this->setWindowFlags(Qt::FramelessWindowHint);

    this->menubar();

    this->populate();

    this->setCentralWidget(central);

    this->menubar()->touch();
}

dummy::~dummy(void)
{

}

void dummy::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    painter.fillRect(event->rect(), QColor(0, 0, 0, 128));
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtkApplication *application = dtkApplication::create(argc, argv);
    application->initialize();

    dtkThemesEngine::instance()->apply("Spacegrey");

    dummy *window = new dummy;
    window->resize(800, 400);
    window->show();
    window->raise();

    dtkWidgetsPassThrough *widget = new dtkWidgetsPassThrough;
    widget->resize(800, 400);
    widget->show();
    widget->raise();

    dtkThemesEngine::instance()->apply("Spacegrey");

    int status = application->exec();

    delete application;
}

// ///////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
