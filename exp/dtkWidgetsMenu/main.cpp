// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>
#include <QtWidgets>

#include <dtkCore>
#include <dtkFonts>
#include <dtkThemes>
#include <dtkWidgets>

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *);

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::core::registerParameters();
    dtk::widgets::initialize("");
    dtkThemesEngine::instance()->apply(); // TODO

    dtkApplication *application = dtkApplication::create(argc, argv);
    application->setApplicationName("dtkWidgetsMenu");
    application->setApplicationVersion("2.0.0");
    application->setOrganizationName("inria");
    application->setOrganizationDomain("fr");
    application->initialize();
    application->setup(setup);

#if 0

// ///////////////////////////////////////////////////////////////////
// Retrieve an arbitrary parameter
// ///////////////////////////////////////////////////////////////////

    if (dtkCoreParameter *parameter = application->window()->menubar()->parameter(":Model parameters 1:prec:"))
        qDebug() << 1 << "Oh yeah" << parameter;

    if (dtkCoreParameter *parameter = application->window()->menubar()->parameter(":Model parameters 1:Model parameters 1.1:prec:"))
        qDebug() << 2 << "Oh yeah" << parameter;

// ///////////////////////////////////////////////////////////////////
// Retrieve an arbitrary parameter widget
// ///////////////////////////////////////////////////////////////////

    if (dtkWidgetsParameter *parameter = application->window()->menubar()->parameterWidget(":Model parameters 1:prec:"))
        qDebug() << 3 << "Oh yeah" << parameter;

    if (dtkWidgetsParameter *parameter = application->window()->menubar()->parameterWidget(":Model parameters 1:Model parameters 1.1:prec:"))
        qDebug() << 4 << "Oh yeah" << parameter;

// ///////////////////////////////////////////////////////////////////

#endif

    return application->exec();
}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

void setup(dtkApplication *application)
{
    QLabel *bg = new QLabel;
    bg->setPixmap(QPixmap(":/main_1.jpg"));

    QScrollArea *area = new QScrollArea(application->window());
    area->setWidget(bg);

    dtkWidgetsMenu *menu_1 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 1");
    dtkWidgetsMenuItem *menuitem_11 = menu_1->addItem(fa::circleo, "Cycle through background");
    menu_1->addItem(fa::circleo, "SubLevel 1-2");
    menu_1->addItem(fa::circleo, "SubLevel 1-3");
    menu_1->addSeparator();
    menu_1->addItem(fa::circleo, "SubLevel 1-4");

    dtkWidgetsMenu *menu_2 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 2");
    menu_2->addItem(fa::circleo, "SubLevel 2-1");
    dtkWidgetsMenu *menu_22 = menu_2->addMenu(fa::circleo, "SubLevel 2-2");
    menu_22->addItem(fa::fileo, "SubSubLevel1");
    menu_22->addItem(fa::fileo, "SubSubLevel2");
    menu_2->addItem(fa::circleo, "Sublevel 2-3");

    dtkWidgetsMenu *menu_3 = new dtkWidgetsMenu(fa::circlethin, "MainLevel 3");
    menu_3->addItem(fa::circleo, "Sublevel 3-1");
    menu_3->addItem(fa::circleo, "Sublevel 3-2");

    QString placeholder = "SubSub";

    dtkWidgetsMenu *m = menu_22->addMenu(fa::home, QString("%1Level3").arg(placeholder));

    for(int i = 0; i < 10; i++) {

        placeholder += "Sub";

        m->addItem(fa::home, QString("%1Level1").arg(placeholder));
        dtkWidgetsMenu *menu = m->addMenu(fa::home, QString("%1").arg(placeholder));
        m->addItem(fa::home, QString("%1Level3").arg(placeholder));
        m->addSeparator();
        m->addItem(fa::home, QString("%1Level4").arg(placeholder));

        m = menu;
    }

    QObject::connect(menuitem_11, &QAction::triggered, [=] (void) -> void
    {
        static int count = 1;

        bg->setPixmap(QPixmap(QString(":/main_%1.jpg").arg((count++ % 5) + 1)));
    });

     dtkWidgetsParameterMenuBarGenerator menubar_generator_1(":parameters_menu.json", ":parameters_definition.json");

    application->window()->menubar()->addMenu(menu_1);
    application->window()->menubar()->addMenu(menu_2);
    application->window()->menubar()->addMenu(menu_3);

    menubar_generator_1.populate(application->window()->menubar());

    application->window()->populate();

    application->window()->setCentralWidget(area);

    application->window()->menubar()->touch();

    QObject::connect(application->window()->menubar(), &dtkWidgetsMenuBar::entered, [=] (dtkWidgetsMenu *target) {
        qDebug() << "Entered:" << target->title();
    });

    QObject::connect(application->window()->menubar(), &dtkWidgetsMenuBar::left, [=] (dtkWidgetsMenu *source) {
        qDebug() << "Left:" << source->title();
    });
}

//
// main.cpp ends here
