// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include  <QtWidgets>
#include <dtkWidgets>

#include <dtkCore/dtkCoreParameter>
#include <dtkWidgets/dtkWidgetsParameter>
#include <dtkWidgets/dtkWidgetsParameterFactory>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    dtk::widgets::initialize("");

    QApplication application(argc, argv);

    QMainWindow *window = new QMainWindow;

    dtk::d_real real_parameter = 5.;
    real_parameter.setDocumentation("A real parameter.");
    real_parameter.setDecimals(3);

    dtkWidgetsParameter *first_real_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&real_parameter, "dtkWidgetsParameterDoubleSpinBox");
    dtkWidgetsParameter *second_real_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&real_parameter, "dtkWidgetsParameterScientificSpinBox");

    real_parameter.connect([=] (QVariant v) {
                               double value = v.value<dtk::d_real>().value();
                               qInfo() << "real_parameter (dbl)=" << value;
                           });

    dtk::d_real scientific_parameter("A scientific parameter", 8.54321, -1.0e+6, +1.0e+6, 6);  // doc, value, min, max, decimals
    //scientific_parameter = 8.654321;
    //scientific_parameter->setDecimals(6);


    dtkWidgetsParameter *scientific_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&scientific_parameter, "dtkWidgetsParameterScientificSpinBox");
    scientific_parameter.connect([=] (QVariant v) {
                               double value = v.value<dtk::d_real>().value();
                               printf("scientific_parameter(XXX) = %.12f\n", value);
                           });

    dtk::d_int longlong_parameter("A long long parameter", 666, 665, 800);  // doc, value, min, max, decimals
    dtkWidgetsParameter *longlong_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&longlong_parameter, "dtkWidgetsParameterLongLongSpinBox");
    longlong_parameter.connect([=] (QVariant v) {
                               long long value = v.value<dtk::d_int>().value();
                               printf("longlong_parameter(XXX) = %lld\n", value);
                           });


    QList<QString> available_strings;
    available_strings.append("FirstString");
    available_strings.append("SecondString");
    available_strings.append("ThirdString");

    dtk::d_inliststring inliststring_parameter("listofstrings", available_strings);
    inliststring_parameter.setDocumentation("A inliststring parameter.");

    inliststring_parameter.setValue(QString("SecondString"));
    dtkWidgetsParameter *inliststring_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&inliststring_parameter, "dtkWidgetsParameterStringListComboBox");

    inliststring_parameter.connect([=] (QVariant v) {
                               QString value = v.value<dtk::d_inliststring>().value();
                               qInfo() << "inliststring_parameter =" << value;
                           });



    dtk::d_bool bool_parameter = true;
    dtkWidgetsParameter *bool_parameter_widget = dtk::widgets::parameters::pluginFactory().create(&bool_parameter, "dtkWidgetsParameterBoolCheckBox");

    bool_parameter.connect([=] (QVariant v) {
                               bool value = v.value<dtk::d_bool>().value();
                               qInfo() << "bool_parameter =" << value;
                           });

    QVBoxLayout *central_layout = new QVBoxLayout;
    QFrame *frame = new QFrame();
    frame->setFrameStyle(QFrame::Box | QFrame::Raised);
    central_layout->addWidget(frame);

    QVBoxLayout *real_layout = new QVBoxLayout;
    frame->setLayout(real_layout);
    real_layout->addWidget(first_real_parameter_widget);
    real_layout->addWidget(second_real_parameter_widget);

    central_layout->addWidget(new QSplitter);
    central_layout->addWidget(scientific_parameter_widget);
    central_layout->addWidget(new QSplitter);
    central_layout->addWidget(longlong_parameter_widget);
    central_layout->addWidget(new QSplitter);
    central_layout->addWidget(inliststring_parameter_widget);
    central_layout->addWidget(new QSplitter);
    central_layout->addWidget(bool_parameter_widget);

    QWidget *central_widget = new QWidget;
    central_widget->setLayout(central_layout);

    window->setCentralWidget(central_widget);

    window->show();
    window->raise();

    return application.exec();
}

//
// main.cpp ends here
