# ChangeLog
## version 2.11.5 - 2019-06-03
- remove hardcoded resize in dtkApp
## version 2.11.4 - 2019-06-03
- fix mouse tracking propagation in dtkApps by calling touch in dtkWidgetsWidget showEvent
## version 2.11.3 - 2019-05-29
- call enter/leave when using workspace menu
- fix main window events on linux
- set ini format in initialize
## version 2.11.2 - 2019-05-27
- settings in ini format per default for dtkApplication
- bugfix in dtkWidgetsParameterFileSave, fileBrowse, boolPushButton
- add menuBar callback
## version 2.11.1 - 2019-05-21
- menubar generator use dtk-core readParameters static method
## version 2.11.0 - 2019-05-21
- menubar generator use dtk-core parameter reader
- add python converter for parameters new json file formats
- fix menubar size
## version 2.10.0 - 2019-05-17
- cmake refactoring
- fix interaction
- better handling of font setting
## version 2.9.2 - 2019-05-03
- do not move window when we are over a menu button
## version 2.9.1 - 2019-05-02
- call enter/leave method when entering/leaving workspaces
- deactivate unused fileMenu and AboutMenu. Move ThemeMenu in a sub-menu
- menubar fixed for HighDpi
## version 2.9.0 - 2019-04-29
- add dtkWidgetsParameterLongLongSpinBox
- use dtk-themes
## version 2.8.0 - 2019-03-29
- add dtkWidgetsParameterFileSave
- add dtkWidgetsParameterScientificSpinBox
- add dtkWidgetsParameterStringLineEdit
## version 2.7.0 - 2019-03-21
- add dtkWidgetsMenu (remove dtkWidgetsOverlayPaneManager and toggle button in Pane)
- add dtk-core as dependancie for new features:
- add Workspace abstraction with factory and manager; add also dtkWidgetsWorkspaceBar and StackBar
- add widgets parameters
## version 2.6.4 - 2019-03-12
- overlayPane dynamically adjust its width
## version 2.6.3 - 2019-02-22
- fix resizing of dtkWidgetsOverlayPane
- do not use hard coded color in dtkWidgetsHUDItem QPainter
## version 2.6.2 - 2019-02-20
- add the possibility to have a toogle button (with tooltip) to dtkWidgetsOverlayPane
- add a dtkWidgetsOverlayPaneManager to have multiple dtkWidgetsOverlayPane with toogle buttons
## version 2.6.0 - 2019-02-14
- allows to disable the footer of the dtkWidgetsLayoutItem
- fix the display of the object name of the view in the footer of the dtkWidgetsLayoutItem
- allows to rename the view from the footer of the dtkWidgetsLayoutItem
- API CHANGE : dtkWidgetsController inserted(dtkWidgetsWidget *, const QString&) to inserted(dtkWidgetsWidget *)
## version 2.5.0 - 2019-02-06
- add method to change size of dtkWidgetsOverlayPane & dtkWidgetsOverlayPaneSlider
## version 2.4.0 - 2019-02-03
- add method to provide and display tooltip on hover of slider bar icons
## version 2.3.0 - 2018-12-10
- add back dtkApplication
- depends on dtk-log
## version 2.2.0 - 2018-10-12
- layout persistence
- add scrolling framework
## version 2.1.3 - 2018-09-20
- fix HUD rendering
- add clear method to OverlayPane
## version 2.1.2 - 2018-09-11
- fix install for windows
## version 2.1.1 - 2018-09-11
- fix EXPORT for windows build
## version 2.1.0 - 2018-07-02
- sdm-modeler release
- add dtkWidgetsStylesheetParser
## version 2.0.0 - 2018-05-18
- initial release
