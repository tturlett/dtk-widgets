// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include "dtkWidgetsParameterMenuBarGeneratorTest.h"

#include <dtkWidgetsTest>

#include <dtkThemes>

#include <dtkWidgets/dtkWidgets>
#include <dtkWidgets/dtkWidgetsParameterMenuBarGenerator>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterMenuBarGeneratorTestCasePrivate
{

};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterMenuBarGeneratorTestCase::dtkWidgetsParameterMenuBarGeneratorTestCase(void) : d(new dtkWidgetsParameterMenuBarGeneratorTestCasePrivate)
{
    dtk::core::registerParameters();

    dtk::widgets::setVerboseLoading(true);
    dtk::widgets::initialize("");
    dtkThemesEngine::instance()->apply();
}

dtkWidgetsParameterMenuBarGeneratorTestCase::~dtkWidgetsParameterMenuBarGeneratorTestCase(void)
{
    delete d;
}

void dtkWidgetsParameterMenuBarGeneratorTestCase::initTestCase(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::init(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::testBasic(void)
{
    QString nature_file_path  = QFINDTESTDATA("../resources/new_menu.json");
    QString definition_file_path  = QFINDTESTDATA("../resources/new_definition.json");

    dtkWidgetsParameterMenuBarGenerator menu_bar_generator(nature_file_path, definition_file_path);

    dtkCoreParameters params = menu_bar_generator.parameters();
    QCOMPARE(params.count() , 78);

    dtkWidgetsMenuBar bar;
    menu_bar_generator.populate(&bar);
    auto menus  = bar.menus();
    QCOMPARE(menus.count() , 4);

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::cleanup(void)
{

}

void dtkWidgetsParameterMenuBarGeneratorTestCase::cleanupTestCase(void)
{

}

DTKWIDGETSTEST_MAIN(dtkWidgetsParameterMenuBarGeneratorTest, dtkWidgetsParameterMenuBarGeneratorTestCase)

//
// dtkWidgetsParameterMenuBarGeneratorTest.cpp ends here
