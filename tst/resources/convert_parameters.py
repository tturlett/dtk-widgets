#!/usr/bin/env python3

import json
import argparse

def parse_group(json_o, type_map):
    for c in json_o["contents"]:
        if 'group' in c:
            parse_group(c['group'], type_map)
        else:
            uid = c['parameter']['uid']
            newtype = c['parameter']['type']
            del c['parameter']['type']
            type_map[uid] = newtype

parser = argparse.ArgumentParser(description='Process Convert dtk parameters file to new format (type in def).')
parser.add_argument('def_file', help='definition file')
parser.add_argument('menu_file', help='menu file')

args = parser.parse_args()

def_data = open(args.def_file)
menu_data = open(args.menu_file)

def_json = json.load(def_data)
menu_json = json.load(menu_data)

type_map = {}
for c in menu_json["contents"]:
     parse_group(c['group'], type_map)

for uid in def_json["contents"].keys():
    if uid in type_map:
        def_json["contents"][uid]['type'] = type_map[uid]
    else:
        print(uid, "not used !")

with open('new_menu.json', 'w') as outfile:
    print("write new_menu.json")
    json.dump(menu_json, outfile, indent = 4,)

with open('new_definition.json', 'w') as outfile:
    print("write new_definition.json")
    json.dump(def_json, outfile, indent = 4,)
