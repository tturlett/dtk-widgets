// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <dtkFonts/dtkFontAwesome>

#include "dtkWidgetsOverlayPaneItem.h"

class DTKWIDGETS_EXPORT dtkWidgetsOverlayPaneSlider : public dtkWidgetsOverlayPaneItem
{
    Q_OBJECT

public:
     dtkWidgetsOverlayPaneSlider(QWidget *parent = nullptr);
    ~dtkWidgetsOverlayPaneSlider(void);

signals:
    void indexVisible(int);
    void finished(void);

#pragma mark - Slide management

public:
    void addSlide(fa::icon, QWidget *);
    void addSlide(fa::icon, QWidget *, const QString&);

public:
    void remSlide(QWidget *);

public:
    void setBound(int);

public:
    void slideTo(int);
    void slideTo(int, std::function<void ()>&);
    void slideToNext(void);
    void slideToPrevious(void);
    void slideToPrevious(std::function<void ()>&);

public:
    void setCurrentIndex(int);
    void setCurrentIndex(int, std::function<void ()>&);

public:
    void enableSpying(bool);

private:
    class dtkWidgetsOverlayPaneSliderPrivate *d;
};

//
// dtkWidgetsOverlayPaneSlider.h ends here
