// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtCore>

#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>

class dtkCoreParameter;
class dtkWidgetsMenu;
class dtkWidgetsParameter;

// /////////////////////////////////////////////////////////////////////////////
// dtkWidgetsMenuBarButton
// /////////////////////////////////////////////////////////////////////////////

class dtkWidgetsMenuBarButton : public QLabel
{
    Q_OBJECT

public:
     dtkWidgetsMenuBarButton(int id, const QString &title, QWidget *parent = nullptr);
    ~dtkWidgetsMenuBarButton(void);

signals:
    void clicked(void);

public slots:
    void touch(bool);

public:
    bool selected(void) const;

public:
    QSize sizeHint(void) const override;

protected:
    void mousePressEvent(QMouseEvent *) override;

    void enterEvent(QEvent *) override;
    void leaveEvent(QEvent *) override;

public:
    dtkWidgetsMenu *menu = nullptr;

private:
    bool m_selected = false;
    int icon_id = 0;
    QMetaObject::Connection m_connection;
};

// /////////////////////////////////////////////////////////////////////////////
// dtkWidgetsMenuBar
// /////////////////////////////////////////////////////////////////////////////

class DTKWIDGETS_EXPORT dtkWidgetsMenuBar : public QFrame
{
    Q_OBJECT

public:
     dtkWidgetsMenuBar(QWidget * = nullptr);
    ~dtkWidgetsMenuBar(void);

signals:
    void clicked(int);

    void entered(dtkWidgetsMenu *);
    void    left(dtkWidgetsMenu *);

#pragma mark - Menu management

public:
    dtkWidgetsMenu *addMenu(dtkWidgetsMenu *);
    dtkWidgetsMenu *addMenu(int, const QString &);

    dtkWidgetsMenu *insertMenu(int, dtkWidgetsMenu *);
    dtkWidgetsMenu *insertMenu(int, int, const QString &);

    void removeMenu(dtkWidgetsMenu *);
    void removeMenu(const QString &);

#pragma mark - Convenience accessors

public:
         dtkWidgetsMenu *menu           (const QString& id);
       dtkCoreParameter *parameter      (const QString& id);
    dtkWidgetsParameter *parameterWidget(const QString& id);

public slots:
    void setCurrentIndex(int);
    void setCurrentIndex(int, std::function<void (void)>&);

public:
    int size(void) const;

public:
    QVector<dtkWidgetsMenu *> menus(void) const;

public slots:
    void setFixedHeight(int);

public slots:
    void touch(void);

protected:
    void resizeEvent(QResizeEvent *)         override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event)  override;

protected:
    class dtkWidgetsMenuBarPrivate *d = nullptr;

protected:
    friend class dtkWidgetsMenuBarContainer;
};

//
// dtkWidgetsMenuBar.h ends here
