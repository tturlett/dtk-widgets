// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

class dtkWidgetsLayout;

class DTKWIDGETS_EXPORT dtkWidgetsLayoutReader
{
public:
     dtkWidgetsLayoutReader(void);
    ~dtkWidgetsLayoutReader(void);

public:
    void setLayout(dtkWidgetsLayout *);

public:
    void read(void);

private:
    class dtkWidgetsLayoutReaderPrivate *d;
};

//
// dtkWidgetsLayoutReader.h ends here
