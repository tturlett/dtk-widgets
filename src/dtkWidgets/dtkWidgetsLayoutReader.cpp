// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsLayout.h"
#include "dtkWidgetsLayoutItem.h"
#include "dtkWidgetsLayoutItem_p.h"
#include "dtkWidgetsLayoutReader.h"

#include <QtCore>
#include <QtWidgets>
#include <QtXml>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsLayoutReaderPrivate
{
public:
    QString locate(const QString& file);

public:
    dtkWidgetsLayout *layout;
};

QString dtkWidgetsLayoutReaderPrivate::locate(const QString& file)
{
    QString path = QStandardPaths::locate(QStandardPaths::AppDataLocation, file);

    if (path.isEmpty()) {
        path += QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first();
        path += QDir::separator();
        path += file;
    }

    return path;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkWidgetsLayoutReader::dtkWidgetsLayoutReader(void) : d(new dtkWidgetsLayoutReaderPrivate)
{

}

dtkWidgetsLayoutReader::~dtkWidgetsLayoutReader(void)
{
    delete d;
}

void dtkWidgetsLayoutReader::setLayout(dtkWidgetsLayout *layout)
{
    d->layout = layout;
}

void dtkWidgetsLayoutReader::read(void)
{
    QFile file(d->locate("layout.dtk"));

    if(!file.open(QIODevice::ReadOnly))
        return;

    QDomDocument document; document.setContent(&file);

    QDomNode root = document.firstChild();

    std::function<void (QDomNode&)> fill;

    fill = [&] (QDomNode& node) {

        if (node.childNodes().count() == 2) {

            dtkWidgetsLayoutItem *c = d->layout->current();

            Qt::Orientation orientation = node.toElement().attribute("o") == "h" ? Qt::Horizontal : Qt::Vertical;

            c->d->splitter->setOrientation(orientation);

            c->split(true);

            dtkWidgetsLayoutItem *a = c->d->a;
            dtkWidgetsLayoutItem *b = c->d->b;

            QDomNode node_a = node.firstChild();
            QDomNode node_b = node.lastChild();

            d->layout->setCurrent(a); fill(node_a);
            d->layout->setCurrent(b); fill(node_b);

        } else {

            QString name = node.toElement().text();

            if(!name.isEmpty())
                d->layout->create(name);

        }
    };

    fill(root);

    file.close();
}

//
// dtkWidgetsLayoutReader.cpp ends here
