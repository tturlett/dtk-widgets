// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterStringListComboBox.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterStringListComboBoxPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterStringListComboBoxPrivate
{
public:
    QComboBox *combo_box = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterStringListComboBox implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterStringListComboBox::dtkWidgetsParameterStringListComboBox(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_inliststring>(parent), d(new dtkWidgetsParameterStringListComboBoxPrivate)
{
    d->combo_box = new QComboBox;

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(d->combo_box);

    this->setLayout(layout);
}

dtkWidgetsParameterStringListComboBox::~dtkWidgetsParameterStringListComboBox(void)
{
    delete d;
}

bool dtkWidgetsParameterStringListComboBox::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_inliststring *>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterStringListComboBox.";
        return false;
    }

    d->combo_box->addItems(m_parameter->values());
    d->combo_box->setToolTip(m_parameter->documentation());

    int index = d->combo_box->findText(m_parameter->value());
    if ( index != -1 ) {
        d->combo_box->setCurrentIndex(index);
    }

    m_parameter->connect([=] (QVariant v) {
        QString value = v.value<dtk::d_inliststring>().value();
        int index = d->combo_box->findText(value);
        if ( index != -1 ) {
            d->combo_box->blockSignals(true);
            d->combo_box->setCurrentIndex(index);
            d->combo_box->blockSignals(false);
        }
    });

    QObject::connect(d->combo_box, &QComboBox::currentTextChanged, [=] (const QString& v)
    {
        m_parameter->setValue(v);
    });

    return true;
}

//
// dtkWidgetsParameterStringListComboBox.cpp ends here
