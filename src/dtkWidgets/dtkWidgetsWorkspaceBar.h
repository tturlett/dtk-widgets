#pragma once

#include <dtkWidgetsExport.h>

#include <QtWidgets/QFrame>

class dtkWidgetsWorkspace;
class QStackedWidget;

class DTKWIDGETS_EXPORT dtkWidgetsWorkspaceBar: public QFrame
{
    Q_OBJECT

public:
    dtkWidgetsWorkspaceBar(QWidget *parent = nullptr);
    virtual ~dtkWidgetsWorkspaceBar();

public:
    void setStack(QStackedWidget *stack);

signals:
    void indexChanged(int);
    void indexDeleted(int);

public slots:
    void addWorkspaceInMenu(const QString& type, const QString& workspace_name = QString(), int icon = 0, const QString& group = QString());

 public:
    void buildFromFactory(void);

public slots:
    void createWorkspace(const QString &name, const QString& type, bool display_destroy = true);

protected:
    void enterEvent(QEvent *);
    void leaveEvent(QEvent *);

public:
    void setCurrentIndex(int);
    void setDynamic(bool);

public:
    QSize sizeHint(void) const;

private:
    class dtkWidgetsWorkspaceBarPrivate *d;
};
