#include "dtkWidgetsWorkspaceBar.h"

#include "dtkWidgets.h"
#include "dtkWidgetsWorkspace.h"

#include <dtkFonts/dtkFontAwesome>

#include <QtGui>
#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// Button
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsWorkspaceBarButton : public QLabel
{
    Q_OBJECT

public:
     dtkWidgetsWorkspaceBarButton(QWidget *parent = nullptr);

public slots:
    void create(QAction *);

protected:
    void mousePressEvent(QMouseEvent *);

public:
    QMenu *menu;
};

dtkWidgetsWorkspaceBarButton::dtkWidgetsWorkspaceBarButton(QWidget *parent): QLabel(parent)
{
    dtkFontAwesome::instance()->setDefaultOption("color", QColor("#ffffff"));

    this->setAlignment(Qt::AlignCenter);
    this->setPixmap(dtkFontAwesome::instance()->icon(fa::plussquare).pixmap(32, 32));
    this->setFixedWidth(32);

    this->menu = new QMenu(this);
    this->menu->setStyleSheet("QMenu::item {padding: 2px 20px 2px 30px; border: 1px solid transparent; spacing: 10px; height: 50px; width: 300px; font-size: 16pt;} QMenu::icon {width: 50px; height:50px;}");

}

void dtkWidgetsWorkspaceBarButton::create(QAction *action)
{
    //TODO
}

void dtkWidgetsWorkspaceBarButton::mousePressEvent(QMouseEvent *event)
{
    this->menu->exec(event->globalPos());
}

// ///////////////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////////////////
// gnomonItemButton
// ///////////////////////////////////////////////////////////////////

class  dtkWidgetsWorkspaceItemButton : public QLabel
{
    Q_OBJECT

public:
    dtkWidgetsWorkspaceItemButton(const QColor& color, int icon, QWidget *parent = nullptr);

signals:
    void clicked(void);

protected:
    void mousePressEvent(QMouseEvent *);

};

dtkWidgetsWorkspaceItemButton::dtkWidgetsWorkspaceItemButton(const QColor& color, int icon, QWidget *parent): QLabel(parent)
{
    dtkFontAwesome::instance()->setDefaultOption("color", color);

    this->setPixmap(dtkFontAwesome::instance()->icon(icon).pixmap(16, 16));

    this->setStyleSheet("background: none; border: none;");
}

void dtkWidgetsWorkspaceItemButton::mousePressEvent(QMouseEvent *e)
{
    emit clicked();
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsWorkspaceBarItem
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsWorkspaceBarItem : public QLabel
{
    Q_OBJECT

public:
    dtkWidgetsWorkspaceBarItem(const QColor& color, const QString& label, QWidget *parent = nullptr, bool display_destroy = true) : QLabel(label, parent) {

        m_color = color;

        if (display_destroy) {

            this->button_destroy = new dtkWidgetsWorkspaceItemButton(color, fa::times, this);
            this->button_destroy->setAlignment(Qt::AlignRight);
            this->button_destroy->setVisible(false);

            connect(this->button_destroy, SIGNAL(clicked()), this, SIGNAL(destroy()));
        }

        this->setAlignment(Qt::AlignCenter);
        this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
        this->setMouseTracking(true);
        this->setStyleSheet(QString("color: rgb(%1,%2,%3);").arg(color.red()).arg(color.green()).arg(color.blue()));

    };

signals:
    void clicked(void);
    void destroy(void);

public:
    dtkWidgetsWorkspaceItemButton *button_destroy = nullptr;

public:
    const QColor& color(void) { return m_color; } ;

protected:
    void mousePressEvent(QMouseEvent *) {
        emit clicked();
    }

    void enterEvent(QEvent *)
    {
        if (this->button_destroy)
            this->button_destroy->setVisible(true);
    }

    void leaveEvent(QEvent *)
    {
        if (this->button_destroy)
            this->button_destroy->setVisible(false);
    }

private:
    QColor m_color;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsWorkspaceBarSeparator
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsWorkspaceBarSeparator : public QLabel
{
public:
    dtkWidgetsWorkspaceBarSeparator(QWidget *parent) : QLabel(parent) {
        dtkFontAwesome::instance()->setDefaultOption("color", QColor("#777777"));

        this->setAlignment(Qt::AlignCenter);
        this->setPixmap(dtkFontAwesome::instance()->icon(fa::chevronright).pixmap(32, 32));
        this->setFixedWidth(32);
    }
};

// ///////////////////////////////////////////////////////////////////
//  dtkWidgetsWorkspaceBarPrivate
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsWorkspaceBarPrivate: public QObject
{
    Q_OBJECT

public:
    dtkWidgetsWorkspaceBar *q;

public:
    QList<dtkWidgetsWorkspaceBarItem *> items;
    QList<dtkWidgetsWorkspaceBarSeparator *> separators;

public:
    dtkWidgetsWorkspaceBarButton *button;

public:
    bool inside = false;

public:
    QHBoxLayout   *layout;
    QStackedWidget *stack;

public slots:
    void onItemClicked(int);

signals:
    void indexDeleted(int);

public:
    void createWorkspace(const QColor &, const QString &, bool display_destroy);

public:
    QList<QColor>    colors;
    QList<QMenu *> submenus;
    QStringList  workspaces;
    dtkWidgetsWorkspace *current_workspace = nullptr;
};

void dtkWidgetsWorkspaceBarPrivate::createWorkspace(const QColor & color, const QString& name, bool display_destroy)
{
    dtkWidgetsWorkspaceBarItem *item = new dtkWidgetsWorkspaceBarItem(color, name, q, display_destroy);

    if (this->items.count() > 0) {
        dtkWidgetsWorkspaceBarSeparator *separator = new dtkWidgetsWorkspaceBarSeparator(q);
        this->layout->insertWidget(this->layout->count()-1, separator);
        this->separators << separator;
    }
    this->layout->insertWidget(this->layout->count()-1, item);

    this->items << item;

    connect(item, &dtkWidgetsWorkspaceBarItem::clicked, [=] () {
        int index =  this->items.indexOf(item);
        this->onItemClicked(index);
    });

    connect(item, &dtkWidgetsWorkspaceBarItem::destroy, [=] () {
        int index =  this->items.indexOf(item);

        this->items.takeAt(index);
        if (index > 0) {
            auto separator = this->separators.takeAt(index-1);
            this->layout->removeWidget(separator);
            delete separator;
        }
        this->layout->removeWidget(item);
        delete item;
        emit indexDeleted(index);
    });

    this->onItemClicked(this->items.count()-1);
}

void dtkWidgetsWorkspaceBarPrivate::onItemClicked(int index)
{
    q->emit indexChanged(index);

    this->stack->setCurrentIndex(index);
    dtkWidgetsWorkspace *workspace = dynamic_cast<dtkWidgetsWorkspace*>(this->stack->currentWidget());
    if (workspace != this->current_workspace) {
        this->current_workspace->leave();
        this->current_workspace = workspace;
        this->current_workspace->enter();
    }

    for (dtkWidgetsWorkspaceBarItem *item : items) {
        int current_index = items.indexOf(item);
        QColor color = this->colors.at(this->workspaces.indexOf(item->text(),0));
        item->setStyleSheet(QString("font-size: %1px; font-style: %2; color: rgb(%3,%4,%5);").arg(current_index == index ? "24" : "12").arg(current_index == index ? "bold" : "normal").arg(color.red()).arg(color.green()).arg(color.blue()));
    }
}

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsWorkspaceBar
// ///////////////////////////////////////////////////////////////////

dtkWidgetsWorkspaceBar::dtkWidgetsWorkspaceBar(QWidget *parent): QFrame(parent), d(new dtkWidgetsWorkspaceBarPrivate)
{
    d->q = this;

    d->colors <<  QColor("#ff3b30") <<  QColor("#89a348") <<  QColor("#a38948") <<  QColor("#ff9500") <<  QColor("#ffcc00") << QColor("#5f9ea0") <<  QColor("#4cd964") <<  QColor("#4c64d9") <<  QColor("#5ac8fa") <<  QColor("#dc143c") << QColor("#dc143c") << QColor("#5856d6") << QColor("#734906");

    d->button = new dtkWidgetsWorkspaceBarButton(this);

    d->layout = new QHBoxLayout(this);
    d->layout->addWidget(d->button);

    connect(d, SIGNAL(indexDeleted(int)), this, SIGNAL(indexDeleted(int)));

    QShortcut *nextWS = new QShortcut(QKeySequence(Qt::ControlModifier + Qt::ShiftModifier + Qt::Key_PageDown), this);
    QShortcut *prevWS = new QShortcut(QKeySequence(Qt::ControlModifier + Qt::ShiftModifier + Qt::Key_PageUp), this);

    connect(nextWS, &QShortcut::activated, [=] (void) {
        int count = d->stack->count();
        int index = (d->stack->currentIndex()+1) % count ;
        this->setCurrentIndex(index);
    });

    connect(prevWS, &QShortcut::activated, [=] (void) {
        int count = d->stack->count();
        int index = (d->stack->currentIndex()+count-1) % count;
        this->setCurrentIndex(index);
    });

    this->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    this->setMouseTracking(true);
}

void dtkWidgetsWorkspaceBar::setStack(QStackedWidget *stack)
{
    d->stack = stack;
}

void dtkWidgetsWorkspaceBar::setDynamic(bool value)
{
    d->button->setVisible(value);
}


void dtkWidgetsWorkspaceBar::addWorkspaceInMenu(const QString & type, const QString& workspace_name, int icon, const QString & group)
{
    if (!dtk::widgets::workspace::pluginFactory().keys().contains(type)) {
        qWarning() << "type" << type << "unknown in factory";
        return;
    }
    QString name = workspace_name;

    if (name.isEmpty())
        name = type;

    if (icon == 0)
        icon = fa::pluscircle;

    d->workspaces << name;

    QMenu *menu = nullptr;
    if (!group.isEmpty()) {

        for (QMenu *smenu: d->submenus) {
            if (smenu->title() == group) {
                menu = smenu;
                break;
            }
        }

        if (!menu) {
            menu = d->button->menu->addMenu(dtkFontAwesome::instance()->icon(fa::image), group);
            menu->setTitle(group);
            d->submenus << menu;
        }
    }

    if (!menu)
        menu = d->button->menu;

    QAction *action = menu->addAction(dtkFontAwesome::instance()->icon(icon), name);

    connect(action, &QAction::triggered, [=] (bool triggered) {
                                             this->createWorkspace(name, type);
                                         });
}

void dtkWidgetsWorkspaceBar::createWorkspace(const QString &name, const QString& type, bool display_destroy)
{
    dtkWidgetsWorkspace *workspace = dtk::widgets::workspace::pluginFactory().create(type);
    workspace->setObjectName(name);

    if (!workspace) {
        qWarning() <<  "can't create workspace of type" << type << dtk::widgets::workspace::pluginFactory().keys();
        return;
    }

    if (!d->workspaces.contains(name))
        d->workspaces << name;

    int index = d->workspaces.indexOf(name) % d->colors.count();
    QColor color = d->colors.at(index) ;

    d->createWorkspace(color, name, display_destroy);

    d->stack->addWidget(workspace);
    d->stack->setCurrentWidget(workspace);
    if (d->current_workspace)
        d->current_workspace->leave();
    workspace->enter();
    d->current_workspace = workspace;
}

dtkWidgetsWorkspaceBar::~dtkWidgetsWorkspaceBar(void)
{
    d->colors.clear();
    d->submenus.clear();
    d->workspaces.clear();

    delete d;
}

void dtkWidgetsWorkspaceBar::enterEvent(QEvent *)
{
    d->inside = true;

    this->update();
}

void dtkWidgetsWorkspaceBar::leaveEvent(QEvent *)
{
    d->inside = false;

    this->update();
}


QSize dtkWidgetsWorkspaceBar::sizeHint(void) const
{
    return QSize(200, 40);
}

void dtkWidgetsWorkspaceBar::setCurrentIndex(int i)
{
    d->onItemClicked(i);
}

void dtkWidgetsWorkspaceBar::buildFromFactory(void)
{
    for (const QString &key: dtk::widgets::workspace::pluginFactory().keys()) {
        this->addWorkspaceInMenu(key);
    }
}

#include "dtkWidgetsWorkspaceBar.moc"
