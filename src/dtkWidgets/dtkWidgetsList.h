// Version: $Id: 16e9f8e3dd70b834ee2bce5b7b776d058c1dce81 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport.h>

#include <QtWidgets/QListWidget>

#include <QtCore/QStringList>

class QMimeData;
class QListWidgetItem;

class DTKWIDGETS_EXPORT dtkWidgetsList : public QListWidget
{
    Q_OBJECT

public:
     dtkWidgetsList(QWidget *parent = nullptr);
    ~dtkWidgetsList(void);

public slots:
    void clear(void);

protected slots:
    void update(void);

protected:
    QMimeData *mimeData(const QList<QListWidgetItem *>& items) const;
    QStringList mimeTypes(void) const;

private:
    class dtkWidgetsListPrivate *d;
};

//
// dtkWidgetsList.h ends here
