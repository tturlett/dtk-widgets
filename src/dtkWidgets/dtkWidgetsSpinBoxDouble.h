// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#pragma once

#include <QtWidgets/QAbstractSpinBox>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetSpinBoxDouble
// ///////////////////////////////////////////////////////////////////
//
// holds both standard and scienti
// . is the separator and does not depend on locale()
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsSpinBoxDouble : public QAbstractSpinBox
{
    Q_OBJECT

public:
     dtkWidgetsSpinBoxDouble(QDoubleValidator::Notation, QWidget * parent = 0);
    ~dtkWidgetsSpinBoxDouble(void);

signals:
    void valueChanged(double);

public:
    void stepBy(int) override;
    void stepDown(void);
    void stepUp(void);

    void setMinimum(double);
    void setMaximum(double);
    void setValue(double);
    void setDecimals(int);

protected:
    QAbstractSpinBox::StepEnabled stepEnabled(void) const override;

private:
    class dtkWidgetsSpinBoxDoublePrivate *d;
};


//
// dtkWidgetsSpinBoxDouble.h ends here
