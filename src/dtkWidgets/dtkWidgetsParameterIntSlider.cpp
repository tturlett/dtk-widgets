// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsParameterIntSlider.h"

#include <QtWidgets>

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterIntSliderPrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsParameterIntSliderPrivate
{
public:
    QSlider *slider = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkWidgetsParameterIntSlider implementation
// ///////////////////////////////////////////////////////////////////

dtkWidgetsParameterIntSlider::dtkWidgetsParameterIntSlider(QWidget* parent) : dtkWidgetsParameterBase<dtk::d_int>(parent), d(new dtkWidgetsParameterIntSliderPrivate)
{
    d->slider = new QSlider;
    d->slider->setOrientation(Qt::Horizontal);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(d->slider);

    this->setLayout(layout);
}

dtkWidgetsParameterIntSlider::~dtkWidgetsParameterIntSlider(void)
{
    delete d;
}

bool dtkWidgetsParameterIntSlider::connect(dtkCoreParameter *p)
{
    if (!p) {
        qWarning() << Q_FUNC_INFO << "The input parameter is null. Nothing is done.";
        return false;
    }

    m_parameter = dynamic_cast<dtk::d_int *>(p);

    if(!m_parameter) {
        qWarning() << Q_FUNC_INFO << "The type of the parameter is not compatible with the widget dtkWidgetsParameterIntSlider.";
        return false;
    }

    d->slider->setMinimum(m_parameter->min());
    d->slider->setMaximum(m_parameter->max());
    d->slider->setToolTip(m_parameter->documentation());

    d->slider->setValue(m_parameter->value());

    m_parameter->connect([=] (QVariant v)
    {
        int value = v.value<dtk::d_int>().value();
        d->slider->blockSignals(true);
        d->slider->setValue(value);
        d->slider->blockSignals(false);
    });

    QObject::connect(d->slider, QOverload<int>::of(&QSlider::valueChanged), [=] (int v)
    {
        m_parameter->setValue(v);
    });

    return true;
}

//
// dtkWidgetsParameterIntSliderWidget.cpp ends here
