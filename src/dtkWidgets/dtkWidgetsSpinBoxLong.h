// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

#include <QtWidgets/QAbstractSpinBox>

class dtkWidgetsSpinBoxLong : public QAbstractSpinBox
{
    Q_OBJECT

public:
     dtkWidgetsSpinBoxLong(QWidget * parent = nullptr);
    ~dtkWidgetsSpinBoxLong(void);

signals:
    void valueChanged(long long);

public:
    void stepBy(int) override;
    void stepDown(void);
    void stepUp(void);

    void setMinimum(long long);
    void setMaximum(long long);
    void setValue(long long);

protected:
    QAbstractSpinBox::StepEnabled stepEnabled(void) const override;

private:
    class dtkWidgetsSpinBoxLongPrivate *d;
};

//
// dtkWidgetsSpinBoxLong.h ends here
