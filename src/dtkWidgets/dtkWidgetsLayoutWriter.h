// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkWidgetsExport>

class dtkWidgetsLayout;

class DTKWIDGETS_EXPORT dtkWidgetsLayoutWriter
{
public:
     dtkWidgetsLayoutWriter(void);
    ~dtkWidgetsLayoutWriter(void);

public:
    void setLayout(dtkWidgetsLayout *);

public:
    void write(void);

private:
    class dtkWidgetsLayoutWriterPrivate *d;
};

//
// dtkWidgetsLayoutWriter.h ends here
