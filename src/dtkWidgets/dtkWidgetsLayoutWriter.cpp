// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWidgetsLayout.h"
#include "dtkWidgetsLayoutItem.h"
#include "dtkWidgetsLayoutItem_p.h"
#include "dtkWidgetsLayoutWriter.h"
#include "dtkWidgetsWidget.h"

#include <QtCore>
#include <QtWidgets>
#include <QtXml>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkWidgetsLayoutWriterPrivate
{
public:
    QString locate(const QString& file);

public:
    dtkWidgetsLayout *layout;
};

QString dtkWidgetsLayoutWriterPrivate::locate(const QString& file)
{
    QString path = QStandardPaths::locate(QStandardPaths::AppDataLocation, file);

    if (path.isEmpty()) {
        path += QStandardPaths::standardLocations(QStandardPaths::AppDataLocation).first();

        QDir dummy; dummy.mkdir(path);

        path += QDir::separator();
        path += file;
    }

    return path;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////


dtkWidgetsLayoutWriter::dtkWidgetsLayoutWriter(void)
{
    d = new dtkWidgetsLayoutWriterPrivate;
}

dtkWidgetsLayoutWriter::~dtkWidgetsLayoutWriter(void)
{
    delete d;
}

void dtkWidgetsLayoutWriter::setLayout(dtkWidgetsLayout *layout)
{
    d->layout = layout;
}

void dtkWidgetsLayoutWriter::write(void)
{
    QFile file(d->locate("layout.dtk"));

    if(!file.open(QIODevice::WriteOnly))
        return;

// ///////////////////////////////////////////////////////////////////
// Build document
// ///////////////////////////////////////////////////////////////////

    QDomDocument document;

    QDomElement r = document.createElement("r");

    if (d->layout->root()->d->a)
        r.setAttribute("o", d->layout->root()->d->splitter->orientation() == Qt::Horizontal ? "h" : "v");

    std::function<void (QDomNode&, dtkWidgetsLayoutItem *)> fill;

    fill = [&] (QDomNode& node, dtkWidgetsLayoutItem *item) {

        if (item->d->a) {

            QDomElement a = document.createElement("a");

            if (item->d->a->d->a && item->d->a->d->b)
                a.setAttribute("o", item->d->splitter->orientation() == Qt::Horizontal ? "v" : "h");

            fill(a, item->d->a);

            node.appendChild(a);
        }

        if (item->d->b) {

            QDomElement b = document.createElement("b");

            if (item->d->b->d->a && item->d->b->d->b)
                b.setAttribute("o", item->d->splitter->orientation() == Qt::Horizontal ? "v" : "h");

            fill(b, item->d->b);

            node.appendChild(b);
        }

        if(!item->d->a && !item->d->b && item->proxy()->d->view) {

            QDomNode p = document.createTextNode(item->proxy()->d->view->objectName());

            node.appendChild(p);
        }
    };

    fill(r, d->layout->root());

    document.appendChild(r);

    QTextStream stream(&file);

    stream << document.toString();

    file.close();
}

//
// dtkWidgetsLayoutWriter.cpp ends here
